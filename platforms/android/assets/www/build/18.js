webpackJsonp([18],{

/***/ 740:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPageModule", function() { return RegisterPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__register__ = __webpack_require__(901);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var RegisterPageModule = (function () {
    function RegisterPageModule() {
    }
    RegisterPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__register__["a" /* RegisterPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__register__["a" /* RegisterPage */]),
                __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], RegisterPageModule);
    return RegisterPageModule;
}());

//# sourceMappingURL=register.module.js.map

/***/ }),

/***/ 901:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_keyboard__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RegisterPage = (function () {
    function RegisterPage(navCtrl, navParams, formBuilder, keyboard, auth, loading, toast, translate, network, menuCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.keyboard = keyboard;
        this.auth = auth;
        this.loading = loading;
        this.toast = toast;
        this.translate = translate;
        this.network = network;
        this.menuCtrl = menuCtrl;
        this.emailValidator = __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required,
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].email
        ]);
        this.passwordValidator = __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required,
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].minLength(4)
        ]);
        this.registerForm = formBuilder.group({
            email: ['', this.emailValidator],
            password: ['', this.passwordValidator],
            confirmPassword: ['', this.passwordValidator]
        });
    }
    RegisterPage.prototype.keyDownFunction = function (event) {
        // User pressed return on keypad, proceed with registration.
        if (event.keyCode == 13) {
            this.keyboard.close();
            this.register();
        }
    };
    RegisterPage.prototype.ionViewDidLoad = function () {
        this.menuCtrl.enable(false);
    };
    RegisterPage.prototype.register = function () {
        var _this = this;
        // Register with Email and Password.
        if (!this.registerForm.valid || this.registerForm.value['password'] != this.registerForm.value['confirmPassword']) {
            this.hasError = true;
        }
        else {
            this.loading.show();
            this.auth.registerWithEmail(this.registerForm.value['email'], this.registerForm.value['password']).then(function (res) {
                _this.loading.hide();
                _this.navCtrl.setRoot('LoaderPage');
                _this.loading.hide();
            }).catch(function (err) {
                _this.toast.show(_this.translate.get(err.code));
                _this.loading.hide();
            });
        }
    };
    RegisterPage.prototype.registerWithFacebook = function () {
        var _this = this;
        // Register with Facebook.
        this.loading.show();
        this.auth.loginWithFacebook().then(function (res) {
            _this.loading.hide();
            _this.navCtrl.setRoot('LoaderPage');
        }).catch(function (err) {
            _this.toast.show(err);
            _this.loading.hide();
        });
    };
    RegisterPage.prototype.registerWithGoogle = function () {
        var _this = this;
        // Register with Google.
        this.loading.show();
        this.auth.loginWithGoogle().then(function (res) {
            _this.loading.hide();
            _this.navCtrl.setRoot('LoaderPage');
        }).catch(function (err) {
            _this.toast.show(err);
            _this.loading.hide();
        });
    };
    RegisterPage.prototype.registerWithTwitter = function () {
        var _this = this;
        // Register with Twitter.
        this.loading.show();
        this.auth.loginWithTwitter().then(function (res) {
            _this.loading.hide();
            _this.navCtrl.setRoot('LoaderPage');
        }).catch(function (err) {
            _this.toast.show(err);
            _this.loading.hide();
        });
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-register',template:/*ion-inline-start:"D:\Fireline\src\pages\auth\register\register.html"*/'<ion-content class="no-scroll">\n  <div class="logo" text-center>\n    <img src="assets/images/logo.png" tappable (click)="navCtrl.setRoot(\'IntroPage\')"/>\n  </div>\n  <div class="form">\n    <form [formGroup]="registerForm" (keydown)="keyDownFunction($event)">\n      <ion-list>\n        <ion-item no-lines>\n          <ion-input type="email" formControlName="email" [placeholder]="translate.get(\'auth.form.email\')"></ion-input>\n        </ion-item>\n        <p text-center *ngIf="(registerForm.controls.email.hasError(\'required\') || !registerForm.controls.email.valid) && hasError">{{ \'auth.form.error.email\' | translate }}</p>\n        <ion-item no-lines>\n          <ion-input type="password" formControlName="password" [placeholder]="translate.get(\'auth.form.password\')"></ion-input>\n        </ion-item>\n        <p text-center *ngIf="(registerForm.controls.password.hasError(\'required\') || !registerForm.controls.password.valid) && hasError">{{ \'auth.form.error.password\' | translate }}</p>\n        <ion-item no-lines>\n          <ion-input type="password" formControlName="confirmPassword" [placeholder]="translate.get(\'auth.form.confirm\')"></ion-input>\n        </ion-item>\n        <p text-center *ngIf="(registerForm.controls.confirmPassword.hasError(\'required\') || !registerForm.controls.confirmPassword.valid) && hasError">{{ \'auth.form.error.confirm\' | translate }}</p>\n        <p text-center *ngIf="(registerForm.controls.confirmPassword.valid && (registerForm.value[\'password\'] != registerForm.value[\'confirmPassword\'])) && hasError">{{ \'auth.form.error.mismatch\' | translate }}</p>\n        <div text-center margin-top>\n          <button ion-button color="primary" (click)="register()" [disabled]="!network.online()"><b>{{ \'auth.button.register\' | translate }}</b></button>\n          <p>{{ \'auth.register.text\' | translate }}</p>\n          <button ion-button color="dark" (click)="registerWithFacebook()" [disabled]="!network.online()"><ion-icon name="icon-facebook"></ion-icon><b>{{ \'auth.button.facebook\' | translate }}</b></button>\n          <!-- <button ion-button color="dark" (click)="registerWithGoogle()" [disabled]="!network.online()"><ion-icon name="icon-google"></ion-icon><b>{{ \'auth.button.google\' | translate }}</b></button> -->\n          <!-- <button ion-button color="dark" (click)="registerWithTwitter()" [disabled]="!network.online()"><ion-icon name="icon-twitter"></ion-icon><b>{{ \'auth.button.twitter\' | translate }}</b></button> -->\n          <p>{{ \'auth.register.question\' | translate }}<span (click)="navCtrl.setRoot(\'LoginPage\')">{{ \'auth.register.question.button\' | translate }}</span></p>\n        </div>\n      </ion-list>\n    </form>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\Fireline\src\pages\auth\register\register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_keyboard__["a" /* Keyboard */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["b" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["d" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["h" /* ToastProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["i" /* TranslateProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["e" /* NetworkProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ })

});
//# sourceMappingURL=18.js.map