webpackJsonp([12],{

/***/ 738:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateProfilePageModule", function() { return CreateProfilePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__create_profile__ = __webpack_require__(899);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_image_loader__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng_elastic__ = __webpack_require__(897);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng_elastic___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng_elastic__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var CreateProfilePageModule = (function () {
    function CreateProfilePageModule() {
    }
    CreateProfilePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__create_profile__["a" /* CreateProfilePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__create_profile__["a" /* CreateProfilePage */]),
                __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["b" /* TranslateModule */].forChild(),
                __WEBPACK_IMPORTED_MODULE_4_ionic_image_loader__["b" /* IonicImageLoader */],
                __WEBPACK_IMPORTED_MODULE_5_ng_elastic__["ElasticModule"]
            ],
        })
    ], CreateProfilePageModule);
    return CreateProfilePageModule;
}());

//# sourceMappingURL=create-profile.module.js.map

/***/ }),

/***/ 889:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(1);
var forms_1 = __webpack_require__(28);
__webpack_require__(173);
__webpack_require__(381);
var Observable_1 = __webpack_require__(0);
var ElasticDirective = /** @class */ (function () {
    function ElasticDirective(element, ngZone, model) {
        this.element = element;
        this.ngZone = ngZone;
        this.model = model;
        this.onResize = new core_1.EventEmitter();
    }
    ElasticDirective.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.model) {
            return;
        }
        // Listen for changes to the underlying model
        // to adjust the textarea size.
        this.modelSub = this.model
            .valueChanges
            .debounceTime(100)
            .subscribe(function () { return _this.adjust(); });
    };
    ElasticDirective.prototype.ngOnDestroy = function () {
        if (this.modelSub) {
            this.modelSub.unsubscribe();
        }
    };
    ElasticDirective.prototype.ngAfterViewInit = function () {
        var _this = this;
        if (this.isTextarea(this.element.nativeElement)) {
            this.setupTextarea(this.element.nativeElement);
            return;
        }
        var children = Array.from(this.element.nativeElement.children);
        var textareaEl = children.find(function (el) { return _this.isTextarea(el); });
        if (textareaEl) {
            this.setupTextarea(textareaEl);
            return;
        }
        throw new Error('The `fz-elastic` attribute directive must be used on a `textarea` or an element that contains a `textarea`.');
    };
    ElasticDirective.prototype.onInput = function () {
        // This is run whenever the user changes the input.
        this.adjust();
    };
    ElasticDirective.prototype.isTextarea = function (el) {
        return el.tagName === 'TEXTAREA';
    };
    ElasticDirective.prototype.setupTextarea = function (textareaEl) {
        var _this = this;
        this.textareaEl = textareaEl;
        // Set some necessary styles
        var style = this.textareaEl.style;
        style.overflow = 'hidden';
        style.resize = 'none';
        // Listen for window resize events
        this.ngZone.runOutsideAngular(function () {
            Observable_1.Observable.fromEvent(window, 'resize')
                .debounceTime(100)
                .subscribe(function () { return _this.adjust(); });
        });
        // Ensure we adjust the textarea if
        // content is already present
        this.adjust();
    };
    ElasticDirective.prototype.adjust = function () {
        if (!this.textareaEl) {
            return;
        }
        var previousHeight = parseInt(this.textareaEl.style.height, 10);
        var newHeight = this.textareaEl.scrollHeight;
        this.textareaEl.style.height = 'auto';
        this.textareaEl.style.height = newHeight + "px";
        if (previousHeight !== newHeight) {
            // send resize event
            this.onResize.emit(newHeight);
        }
    };
    ElasticDirective.decorators = [
        { type: core_1.Directive, args: [{
                    selector: '[fz-elastic]'
                },] },
    ];
    /** @nocollapse */
    ElasticDirective.ctorParameters = function () { return [
        { type: core_1.ElementRef, },
        { type: core_1.NgZone, },
        { type: forms_1.NgModel, decorators: [{ type: core_1.Optional },] },
    ]; };
    ElasticDirective.propDecorators = {
        'onResize': [{ type: core_1.Output, args: ['on-resize',] },],
        'onInput': [{ type: core_1.HostListener, args: ['input',] },],
    };
    return ElasticDirective;
}());
exports.ElasticDirective = ElasticDirective;


/***/ }),

/***/ 890:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user__ = __webpack_require__(891);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_0__user__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__message__ = __webpack_require__(892);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_1__message__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__conversation__ = __webpack_require__(893);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_2__conversation__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__group__ = __webpack_require__(894);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_3__group__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_conversation__ = __webpack_require__(895);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_4__user_conversation__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__user_group__ = __webpack_require__(896);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_5__user_group__["a"]; });
// Add your models here for easy indexing.






//# sourceMappingURL=index.js.map

/***/ }),

/***/ 891:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
var User = (function () {
    function User(userId, email, firstName, lastName, photo, username, bio, contacts, //userIds of contacts
        requestsSent, //userIds whom you sent a contact request
        requestsReceived, //userIds who sent you a contact request
        conversations, groups, pushToken, notifications) {
        this.userId = userId;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.photo = photo;
        this.username = username;
        this.bio = bio;
        this.contacts = contacts;
        this.requestsSent = requestsSent;
        this.requestsReceived = requestsReceived;
        this.conversations = conversations;
        this.groups = groups;
        this.pushToken = pushToken;
        this.notifications = notifications;
        this.object = {
            userId: userId,
            email: this.email,
            firstName: this.firstName,
            lastName: this.lastName,
            photo: this.photo,
            username: this.username,
            bio: this.bio,
            requestsSent: this.requestsSent,
            requestsReceived: this.requestsReceived,
            pushToken: this.pushToken,
            notifications: this.notifications
        };
    }
    return User;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 892:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Message; });
var Message = (function () {
    function Message(sender, //userId of sender
        type, //0 = text, 1 = image
        message, date) {
        this.sender = sender;
        this.type = type;
        this.message = message;
        this.date = date;
        this.object = {
            sender: sender,
            type: type,
            message: message,
            date: date
        };
    }
    return Message;
}());

//# sourceMappingURL=message.js.map

/***/ }),

/***/ 893:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Conversation; });
var Conversation = (function () {
    function Conversation(conversationId, messages //based on Message Model
    ) {
        this.conversationId = conversationId;
        this.messages = messages; //based on Message Model
        this.object = {
            conversationId: conversationId,
            messages: messages
        };
    }
    return Conversation;
}());

//# sourceMappingURL=conversation.js.map

/***/ }),

/***/ 894:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Group; });
var Group = (function () {
    function Group(groupId, title, photo, members, //userIds of members of the group
        messages, //based on Message Model
        participants //userIds on users who has sent atleast one message
    ) {
        this.groupId = groupId;
        this.title = title;
        this.photo = photo;
        this.members = members;
        this.messages = messages;
        this.participants = participants; //userIds on users who has sent atleast one message
        this.object = {
            groupId: groupId,
            title: title,
            photo: photo,
            members: members,
            messages: messages,
            participants: participants
        };
    }
    return Group;
}());

//# sourceMappingURL=group.js.map

/***/ }),

/***/ 895:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserConversation; });
var UserConversation = (function () {
    function UserConversation(conversationId, messagesRead) {
        this.conversationId = conversationId;
        this.messagesRead = messagesRead;
        this.object = {
            conversationId: conversationId,
            messagesRead: messagesRead
        };
    }
    return UserConversation;
}());

//# sourceMappingURL=user-conversation.js.map

/***/ }),

/***/ 896:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserGroup; });
var UserGroup = (function () {
    function UserGroup(groupId, messagesRead) {
        this.groupId = groupId;
        this.messagesRead = messagesRead;
        this.object = {
            groupId: groupId,
            messagesRead: messagesRead
        };
    }
    return UserGroup;
}());

//# sourceMappingURL=user-group.js.map

/***/ }),

/***/ 897:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var elastic_module_1 = __webpack_require__(898);
exports.ElasticModule = elastic_module_1.ElasticModule;
var elastic_directive_1 = __webpack_require__(889);
exports.ElasticDirective = elastic_directive_1.ElasticDirective;


/***/ }),

/***/ 898:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(1);
var elastic_directive_1 = __webpack_require__(889);
var ElasticModule = /** @class */ (function () {
    function ElasticModule() {
    }
    ElasticModule.decorators = [
        { type: core_1.NgModule, args: [{
                    declarations: [elastic_directive_1.ElasticDirective],
                    exports: [elastic_directive_1.ElasticDirective]
                },] },
    ];
    /** @nocollapse */
    ElasticModule.ctorParameters = function () { return []; };
    return ElasticModule;
}());
exports.ElasticModule = ElasticModule;


/***/ }),

/***/ 899:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_keyboard__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_device__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__models__ = __webpack_require__(890);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var CreateProfilePage = (function () {
    function CreateProfilePage(navCtrl, navParams, menuCtrl, actionSheetCtrl, formBuilder, auth, translate, firestore, loading, storage, network, notification, keyboard, camera, device, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.formBuilder = formBuilder;
        this.auth = auth;
        this.translate = translate;
        this.firestore = firestore;
        this.loading = loading;
        this.storage = storage;
        this.network = network;
        this.notification = notification;
        this.keyboard = keyboard;
        this.camera = camera;
        this.device = device;
        this.platform = platform;
        this.photo = 'assets/images/profile.png';
        this.nameValidator = __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required
        ]);
        this.usernameValidator = __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].pattern('^[0-z.]{4,20}$'),
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required
        ]);
        this.emailValidator = __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required,
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].email
        ]);
        this.bioValidator = __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required
        ]);
        this.profileForm = formBuilder.group({
            firstName: ['', this.nameValidator],
            lastName: ['', this.nameValidator],
            username: ['', this.usernameValidator],
            email: ['', this.emailValidator],
            bio: ['', this.bioValidator]
        });
    }
    CreateProfilePage.prototype.keyDownFunction = function (event) {
        // User pressed return on keypad, proceed with creating profile.
        if (event.keyCode == 13) {
            this.keyboard.close();
            this.createProfile();
        }
    };
    CreateProfilePage.prototype.onInput = function (username) {
        var _this = this;
        // Check if the username entered on the form is still available.
        this.uniqueUsername = true;
        if (this.profileForm.controls.username.valid && !this.profileForm.controls.username.hasError('required')) {
            this.firestore.getUserByUsername('@' + username.toLowerCase()).then(function (user) {
                if (user) {
                    _this.uniqueUsername = false;
                }
            }).catch(function () { });
        }
    };
    CreateProfilePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Check if device is running on android and adjust the scss accordingly.
            if (_this.device.platform == 'Android') {
                _this.android = true;
            }
            else {
                _this.android = false;
            }
        }).catch(function () { });
        // Disable sideMenu.
        this.menuCtrl.enable(false);
        // Fill up the form with relevant user info based on the authenticated user on Firebase.
        this.auth.getUser().then(function (user) {
            _this.userId = user.uid;
            if (user.photoURL) {
                _this.photo = user.photoURL;
            }
            var firstName = '';
            var lastName = '';
            if (user.displayName) {
                firstName = user.displayName.substr(0, user.displayName.indexOf(' '));
                lastName = user.displayName.substr(user.displayName.indexOf(' ') + 1, user.displayName.length);
            }
            _this.profileForm.setValue({
                firstName: firstName,
                lastName: lastName,
                username: '',
                email: user.email
            });
        }).catch(function () { });
    };
    CreateProfilePage.prototype.ionViewWillUnload = function () {
        var _this = this;
        // Check if userData exists on Firestore. If no userData exists yet, delete the photo uploaded to save Firebase storage space.
        this.firestore.exists('users/' + this.userId).then(function (exists) {
            if (!exists) {
                _this.storage.delete(_this.userId, _this.photo);
            }
        }).catch(function () { });
    };
    CreateProfilePage.prototype.createProfile = function () {
        var _this = this;
        // Check if profileForm is valid and username is unique and proceed with creating the profile.
        if (!this.profileForm.valid || !this.uniqueUsername) {
            this.hasError = true;
        }
        else {
            if (this.uniqueUsername) {
                this.loading.show();
                // Create userData on Firestore.
                this.firestore.get('users/' + this.userId).then(function (ref) {
                    // Formatting the first and last names to capitalized.
                    var firstName = _this.profileForm.value['firstName'].charAt(0).toUpperCase() + _this.profileForm.value['firstName'].slice(1).toLowerCase();
                    var lastName = _this.profileForm.value['lastName'].charAt(0).toUpperCase() + _this.profileForm.value['lastName'].slice(1).toLowerCase();
                    var user = new __WEBPACK_IMPORTED_MODULE_7__models__["d" /* User */](_this.userId, _this.profileForm.value['email'].toLowerCase(), firstName, lastName, _this.photo, '@' + _this.profileForm.value['username'].toLowerCase(), _this.profileForm.value['bio'], null, null, null, null, null, '', true);
                    ref.set(user.object).then(function () {
                        _this.notification.init();
                        _this.loading.hide();
                        _this.navCtrl.setRoot('LoaderPage');
                    }).catch(function () { });
                }).catch(function () { });
            }
        }
    };
    CreateProfilePage.prototype.setPhoto = function () {
        var _this = this;
        // Allow user to upload and set their profile photo using their camera or photo gallery.
        if (this.network.online()) {
            this.actionSheetCtrl.create({
                title: this.translate.get('auth.profile.photo.title'),
                buttons: [
                    {
                        text: this.translate.get('auth.profile.photo.take'),
                        role: 'destructive',
                        handler: function () {
                            _this.storage.upload(_this.userId, _this.storage.profilePhoto, _this.camera.PictureSourceType.CAMERA).then(function (url) {
                                _this.storage.delete(_this.userId, _this.photo);
                                _this.photo = url;
                            }).catch(function () { });
                        }
                    },
                    {
                        text: this.translate.get('auth.profile.photo.gallery'),
                        handler: function () {
                            _this.storage.upload(_this.userId, _this.storage.profilePhoto, _this.camera.PictureSourceType.PHOTOLIBRARY).then(function (url) {
                                _this.storage.delete(_this.userId, _this.photo);
                                _this.photo = url;
                            }).catch(function () { });
                        }
                    },
                    {
                        text: this.translate.get('auth.profile.photo.cancel'),
                        role: 'cancel',
                        handler: function () { }
                    }
                ]
            }).present();
        }
    };
    CreateProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-create-profile',template:/*ion-inline-start:"D:\Fireline\src\pages\auth\create-profile\create-profile.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title><b>{{ \'auth.nav.title.create\' | translate }}</b></ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content [ngClass]="{\'android\': android}">\n  <div class="profile" text-center>\n    <img-loader [src]="photo" tappable (click)="setPhoto()"></img-loader>\n  </div>\n  <div class="form">\n    <form [formGroup]="profileForm" (keydown)="keyDownFunction($event)">\n      <ion-list>\n        <ion-item no-lines margin-bottom>\n          <ion-input type="text" formControlName="firstName" [placeholder]="translate.get(\'auth.form.firstName\')"></ion-input>\n        </ion-item>\n        <p text-center *ngIf="(profileForm.controls.firstName.hasError(\'required\') || !profileForm.controls.firstName.valid) && hasError">{{ \'auth.form.error.firstName\' | translate }}</p>\n        <ion-item no-lines margin-bottom>\n          <ion-input type="text" formControlName="lastName" [placeholder]="translate.get(\'auth.form.lastName\')"></ion-input>\n        </ion-item>\n        <p text-center *ngIf="(profileForm.controls.lastName.hasError(\'required\') || !profileForm.controls.lastName.valid) && hasError">{{ \'auth.form.error.lastName\' | translate }}</p>\n        <ion-item no-lines margin-bottom>\n          <ion-input type="text" formControlName="username" [placeholder]="translate.get(\'auth.form.username\')" (input)="onInput($event.target.value)"></ion-input>\n        </ion-item>\n        <p text-center *ngIf="(profileForm.controls.username.hasError(\'required\') || !profileForm.controls.username.valid) && hasError">{{ \'auth.form.error.username\' | translate }}</p>\n        <p text-center *ngIf="!uniqueUsername && profileForm.controls.username.valid && hasError">{{ \'auth.form.error.exists\' | translate }}</p>\n        <ion-item no-lines margin-bottom>\n          <ion-input type="text" formControlName="email" [placeholder]="translate.get(\'auth.form.email\')"></ion-input>\n        </ion-item>\n        <p text-center *ngIf="(profileForm.controls.email.hasError(\'required\') || !profileForm.controls.email.valid) && hasError">{{ \'auth.form.error.email\' | translate }}</p>\n        <ion-item no-lines margin-bottom class="textarea">\n          <ion-textarea type="text" formControlName="bio" [placeholder]="translate.get(\'auth.form.bio\')" fz-elastic rows="3"></ion-textarea>\n        </ion-item>\n        <p text-center *ngIf="profileForm.controls.bio.hasError(\'required\') && hasError">{{ \'auth.form.error.bio\' | translate }}</p>\n        <div class="button" text-center>\n          <button ion-button color="primary" (click)="createProfile()" [disabled]="!network.online()"><b>{{ \'auth.button.create\' | translate }}</b></button>\n        </div>\n      </ion-list>\n    </form>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\Fireline\src\pages\auth\create-profile\create-profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers__["b" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["i" /* TranslateProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["c" /* FirestoreProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["d" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["g" /* StorageProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["e" /* NetworkProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["f" /* NotificationProvider */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_keyboard__["a" /* Keyboard */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_device__["a" /* Device */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */]])
    ], CreateProfilePage);
    return CreateProfilePage;
}());

//# sourceMappingURL=create-profile.js.map

/***/ })

});
//# sourceMappingURL=12.js.map