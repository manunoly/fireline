webpackJsonp([16],{

/***/ 393:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupInfoPageModule", function() { return GroupInfoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__group_info__ = __webpack_require__(558);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_image_loader__ = __webpack_require__(125);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var GroupInfoPageModule = (function () {
    function GroupInfoPageModule() {
    }
    GroupInfoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__group_info__["a" /* GroupInfoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__group_info__["a" /* GroupInfoPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild(),
                __WEBPACK_IMPORTED_MODULE_4_ionic_image_loader__["b" /* IonicImageLoader */]
            ],
        })
    ], GroupInfoPageModule);
    return GroupInfoPageModule;
}());

//# sourceMappingURL=group-info.module.js.map

/***/ }),

/***/ 558:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupInfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_device__ = __webpack_require__(126);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var GroupInfoPage = (function () {
    function GroupInfoPage(navCtrl, navParams, firestore, auth, loading, translate, network, storage, actionSheetCtrl, alertCtrl, modalCtrl, camera, alert, platform, device) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.firestore = firestore;
        this.auth = auth;
        this.loading = loading;
        this.translate = translate;
        this.network = network;
        this.storage = storage;
        this.actionSheetCtrl = actionSheetCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.camera = camera;
        this.alert = alert;
        this.platform = platform;
        this.device = device;
    }
    GroupInfoPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Check if device is on iPhoneX and adjust the scss accordingly.
            if (_this.device.model.indexOf('iPhone10') > -1) {
                _this.iPhoneX = true;
            }
            else {
                _this.iPhoneX = false;
            }
            // Check if device is running on android and adjust the scss accordingly.
            if (_this.device.platform == 'Android') {
                _this.android = true;
            }
            else {
                _this.android = false;
            }
        }).catch(function () { });
        this.subscriptions = [];
        if (!this.groupId) {
            this.groupId = this.navParams.get('groupId');
        }
        this.loading.show();
        // Subscribe to group from Firestore and sync.
        this.firestore.get('groups/' + this.groupId).then(function (ref) {
            var subscription = ref.valueChanges().subscribe(function (group) {
                if (group) {
                    _this.group = group;
                    _this.setUsers();
                    // Check if user is a member of the group, if not, pop the view.
                    if (_this.group.members.indexOf(_this.auth.getUserData().userId) == -1) {
                        _this.navCtrl.pop();
                    }
                }
                else {
                    // Group is deleted, pop the view.
                    _this.navCtrl.pop();
                }
                _this.loading.hide();
            });
            _this.subscriptions.push(subscription);
        }).catch(function () {
            _this.loading.hide();
        });
    };
    GroupInfoPage.prototype.ionViewWillUnload = function () {
        // Clear subscriptions.
        if (this.subscriptions) {
            for (var i = 0; i < this.subscriptions.length; i++) {
                this.subscriptions[i].unsubscribe();
            }
        }
    };
    GroupInfoPage.prototype.header = function (record, recordIndex, records) {
        if (recordIndex == 0) {
            return record;
        }
        return null;
    };
    // Subscribe to members of the group.
    GroupInfoPage.prototype.setUsers = function () {
        var _this = this;
        this.users = [];
        for (var i = 0; i < this.group.members.length; i++) {
            var userId = this.group.members[i];
            this.firestore.get('users/' + userId).then(function (ref) {
                var subscription = ref.valueChanges().subscribe(function (user) {
                    _this.addOrUpdateUser(user);
                });
                _this.subscriptions.push(subscription);
            }).catch(function () { });
        }
    };
    // Add or update user data to sync with Firestore.
    GroupInfoPage.prototype.addOrUpdateUser = function (user) {
        if (this.users) {
            var index = -1;
            for (var i = 0; i < this.users.length; i++) {
                if (user.userId == this.users[i].userId) {
                    index = i;
                }
            }
            if (index > -1) {
                this.users[index] = user;
            }
            else {
                this.users.push(user);
            }
        }
        else {
            this.users = [user];
        }
    };
    GroupInfoPage.prototype.setPhoto = function () {
        var _this = this;
        // Allow member to upload and set the group photo using their camera or photo gallery.
        if (this.network.online()) {
            this.actionSheetCtrl.create({
                title: this.translate.get('group.photo.title'),
                buttons: [
                    {
                        text: this.translate.get('auth.profile.photo.take'),
                        role: 'destructive',
                        handler: function () {
                            // Take a photo.
                            _this.storage.upload(_this.groupId, _this.storage.profilePhoto, _this.camera.PictureSourceType.CAMERA).then(function (url) {
                                var toDelete = _this.group.photo;
                                _this.group.photo = url;
                                // Update group photo on Firestore.
                                _this.firestore.get('groups/' + _this.groupId).then(function (ref) {
                                    ref.update({
                                        photo: url
                                    });
                                });
                                // Delete old group photo.
                                _this.storage.delete(_this.groupId, toDelete);
                            }).catch(function () { });
                        }
                    },
                    {
                        text: this.translate.get('auth.profile.photo.gallery'),
                        handler: function () {
                            // Choose from photo gallery.
                            _this.storage.upload(_this.groupId, _this.storage.profilePhoto, _this.camera.PictureSourceType.PHOTOLIBRARY).then(function (url) {
                                var toDelete = _this.group.photo;
                                _this.group.photo = url;
                                // Update group photo on Firestore.
                                _this.firestore.get('groups/' + _this.groupId).then(function (ref) {
                                    ref.update({
                                        photo: url
                                    });
                                });
                                // Delete old group photo.
                                _this.storage.delete(_this.groupId, toDelete);
                            }).catch(function () { });
                        }
                    },
                    {
                        text: this.translate.get('auth.profile.photo.cancel'),
                        role: 'cancel',
                        handler: function () { }
                    }
                ]
            }).present();
        }
    };
    GroupInfoPage.prototype.setTitle = function () {
        var _this = this;
        // Allow user to change the group title.
        if (this.network.online()) {
            var alert_1 = this.alertCtrl.create({
                title: this.translate.get('group.set.title'),
                inputs: [
                    {
                        name: 'title',
                        placeholder: this.translate.get('group.set.group.title'),
                        type: 'text',
                        value: this.group.title
                    }
                ],
                buttons: [
                    {
                        text: this.translate.get('auth.profile.password.button.cancel'),
                        role: 'cancel',
                        handler: function (data) { }
                    },
                    {
                        text: this.translate.get('auth.profile.password.button.save'),
                        handler: function (data) {
                            // Check if the user has entered a group title.
                            if (data.title) {
                                _this.loading.show();
                                // Update group data on Firestore.
                                _this.firestore.get('groups/' + _this.groupId).then(function (ref) {
                                    ref.update({
                                        title: data.title
                                    }).then(function () {
                                        _this.loading.hide();
                                    });
                                });
                            }
                            else {
                                return false;
                            }
                        }
                    }
                ]
            });
            alert_1.present();
        }
    };
    // View Profile of the user given the userId.
    GroupInfoPage.prototype.viewProfile = function (userId) {
        var _this = this;
        var modal = this.modalCtrl.create('ViewProfilePage', { userId: userId });
        modal.present();
        modal.onDidDismiss(function (userId) {
            if (userId) {
                _this.modalCtrl.create('ChatPage', { userId: userId }).present();
            }
        });
    };
    GroupInfoPage.prototype.leave = function () {
        var _this = this;
        // Confirm if the user wants to leave the group.
        this.alert.showConfirm(this.translate.get('group.alert.leave.title'), this.translate.get('group.alert.leave.text') + ' <b>' + this.group.title + '</b>?', this.translate.get('group.alert.leave.button.cancel'), this.translate.get('group.alert.leave.button.leave')).then(function (confirm) {
            if (confirm) {
                var userId = _this.auth.getUserData().userId;
                // Remove the userGroups reference of the user.
                _this.firestore.get('users/' + userId + '/groups/' + _this.groupId).then(function (ref) {
                    ref.delete();
                });
                // Update group members on Firestore.
                _this.group.members.splice(_this.group.members.indexOf(userId), 1);
                _this.firestore.get('groups/' + _this.groupId).then(function (ref) {
                    ref.update({
                        members: _this.group.members
                    }).then(function () { }).catch(function () { });
                }).catch(function () { });
            }
        }).catch(function () { });
    };
    GroupInfoPage.prototype.delete = function () {
        var _this = this;
        // Confirm if the user wants to delete the group.
        this.alert.showConfirm(this.translate.get('group.alert.delete.title'), this.translate.get('group.alert.delete.text') + ' <b>' + this.group.title + '</b>?', this.translate.get('group.alert.delete.button.cancel'), this.translate.get('group.alert.delete.button.delete')).then(function (confirm) {
            if (confirm) {
                // Delete all image messages on storage.
                for (var i = 0; i < _this.group.messages.length; i++) {
                    var message = _this.group.messages[i];
                    if (message.type == 1) {
                        _this.storage.delete(message.sender, message.message);
                    }
                }
                // Remove the userGroups reference of the user.
                _this.firestore.get('users/' + _this.auth.getUserData().userId + '/groups/' + _this.groupId).then(function (ref) {
                    ref.delete().then(function () {
                        // Delete group on Firestore.
                        _this.firestore.get('groups/' + _this.groupId).then(function (ref) {
                            ref.delete().then(function () {
                                _this.group = null;
                            });
                        });
                    });
                });
            }
        }).catch(function () { });
    };
    GroupInfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-group-info',template:/*ion-inline-start:"D:\Fireline\src\pages\main\group-info\group-info.html"*/'<ion-header>\n  <ion-navbar hideBackButton="true">\n    <ion-title *ngIf="group"><b>{{ group.title }}</b></ion-title>\n    <ion-buttons start>\n      <button ion-button tappable (click)="navCtrl.pop()"><ion-icon name="icon-close"></ion-icon></button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content [ngClass]="{\'android\': android}">\n  <div class="profile" text-center>\n    <img-loader [src]="group.photo" tappable (click)="setPhoto()" *ngIf="group"></img-loader>\n  </div>\n  <ion-list class="form" *ngIf="group">\n    <ion-item no-lines>\n      <ion-input type="text" [(ngModel)]="group.title" disabled></ion-input>\n    </ion-item>\n    <button ion-button item-end (click)="setTitle()" [disabled]="!network.online()"><ion-icon name="icon-edit"></ion-icon></button>\n  </ion-list>\n  <ion-list class="users" [virtualScroll]="users" [headerFn]="header" approxItemHeight="60px">\n    <ion-item-divider *virtualHeader="let header"><b>{{ \'group.members\' | translate }}</b></ion-item-divider>\n    <ion-item no-lines *virtualItem="let user" tappable (click)="viewProfile(user.userId)">\n      <ion-avatar item-start>\n        <img-loader [src]="user?.photo"></img-loader>\n      </ion-avatar>\n      <h2 text-uppercase>{{ user?.firstName }} {{ user?.lastName }}</h2>\n      <p text-lowercase>{{ user?.username }}</p>\n    </ion-item>\n  </ion-list>\n</ion-content>\n<ion-footer [ngClass]="{\'iPhoneX\': iPhoneX}">\n  <div text-center>\n    <button ion-button color="dark" *ngIf="group && group.members[0] == auth.getUserData().userId" (click)="modalCtrl.create(\'GroupMembersPage\', { groupId: groupId }).present()" [disabled]="!network.online()"><b>{{ \'group.manage.members\' | translate }}</b></button>\n    <button ion-button color="dark" *ngIf="group && group.members[0] != auth.getUserData().userId" (click)="modalCtrl.create(\'AddMemberPage\', { groupId: groupId }).present()" [disabled]="!network.online()"><b>{{ \'group.add.members\' | translate }}</b></button>\n    <button ion-button color="primary" *ngIf="group && group.members[0] != auth.getUserData().userId" (click)="leave()" [disabled]="!network.online()"><b>{{ \'group.leave.group\' | translate }}</b></button>\n    <button ion-button color="primary" *ngIf="group && group.members[0] == auth.getUserData().userId && group.members.length == 1" (click)="delete()" [disabled]="!network.online()"><b>{{ \'group.delete.group\' | translate }}</b></button>\n  </div>\n</ion-footer>\n'/*ion-inline-end:"D:\Fireline\src\pages\main\group-info\group-info.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers__["c" /* FirestoreProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers__["b" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers__["d" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers__["i" /* TranslateProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers__["e" /* NetworkProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers__["g" /* StorageProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_2__providers__["a" /* AlertProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_device__["a" /* Device */]])
    ], GroupInfoPage);
    return GroupInfoPage;
}());

//# sourceMappingURL=group-info.js.map

/***/ })

});
//# sourceMappingURL=16.js.map