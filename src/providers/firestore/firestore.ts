import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import 'rxjs/add/operator/take';
import { User, Conversation } from '../../models';

@Injectable()
export class FirestoreProvider {
  constructor(private afs: AngularFirestore) { }

  // Obtiene un objeto de Firestore por su ruta. Por ejemplo: firerestore.get ('users /' + userId) para obtener un objeto de usuario.
  public get(path: string): Promise<AngularFirestoreDocument<{}>> {
    return new Promise(resolve => {
      resolve(this.afs.doc(path));
    });
  }

  // Verifica si el objeto existe en Firestore. Devuelve una promesa booleana con verdadero / falso.
  public exists(path: string): Promise<boolean> {
    return new Promise(resolve => {
      this.afs.doc(path).valueChanges().take(1).subscribe(res => {
        if (res) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  }

  // Obtenga todos los usuarios en orden Firestore por sus primeros Nombres.
  public getUsers(): AngularFirestoreCollection<User> {
    return this.afs.collection('users', ref => ref.orderBy('firstName'));
  }

  // Obtener todas las conversaciones en Firestore.
  public getConversations(): AngularFirestoreCollection<{}> {
    return this.afs.collection('conversations');
  }

  // Obtenga todas las conversaciones del usuario en Firestore.
  public getUserConversations(userId: string): AngularFirestoreCollection<{}> {
    return this.afs.collection('users/' + userId + '/conversations');
  }

  // Obtener información de conversación de un usuario con su compañero en Firestore.
  public getUserConversation(userId: string, partnerId: string): AngularFirestoreDocument<{}> {
    return this.afs.doc('users/' + userId + '/conversations/' + partnerId);
  }

  // Obtener todos los grupos en Firestore.
  public getGroups(): AngularFirestoreCollection<{}> {
    return this.afs.collection('groups');
  }

  // Obtenga todos los grupos de usuarios en Firestore.
  public getUserGroups(userId: string): AngularFirestoreCollection<{}> {
    return this.afs.collection('users/' + userId + '/groups');
  }

  // Obtener información de grupo de un usuario con el groupId en Firestore.
  public getUserGroup(userId: string, groupId: string): AngularFirestoreDocument<{}> {
    return this.afs.doc('users/' + userId + '/groups/' + groupId);
  }

  // Obtener userData de un usuario dado el nombre de usuario. Devuelve la promesa userData.
  public getUserByUsername(username: string): Promise<User> {
    return new Promise(resolve => {
      this.afs.collection('users', ref => ref.where('username', '==', username)).valueChanges().take(1).subscribe((res: User[]) => {
        if (res.length > 0) {
          resolve(res[0]);
        } else {
          resolve();
        }
      });
    });
  }

  // Obtiene userData de un usuario dado el pushToken. Devuelve la promesa userData.
  public getUserByPushToken(token: string): Promise<User> {
    return new Promise(resolve => {
      this.afs.collection('users', ref => ref.where('pushToken', '==', token)).valueChanges().take(1).subscribe((res: User[]) => {
        if (res.length > 0) {
          resolve(res[0]);
        } else {
          resolve();
        }
      });
    });
  }

  // Establece el pushToken del usuario dado el userId.
  public setPushToken(userId: string, token: string): void {
    this.get('users/' + userId).then(ref => {
      ref.update({
        pushToken: token
      });
    }).catch(() => { });
  }

  // Eliminar el pushToken del usuario dado el userId.
  public removePushToken(userId: string): void {
    this.get('users/' + userId).then(ref => {
      ref.update({
        pushToken: ''
      });
    }).catch(() => { });
  }

  // Enviar una solicitud de contacto dado el ID de usuario emisor y receptor.
  public sendRequest(from: string, to: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.get('users/' + from).then(ref => {
        ref.valueChanges().take(1).subscribe((user: User) => {
          if (!user.requestsSent) {
            user.requestsSent = [to];
          } else {
            if (user.requestsSent.indexOf(to) == -1) {
              user.requestsSent.push(to);
            }
          }
          ref.update({
            requestsSent: user.requestsSent
          }).then(() => {
            this.get('users/' + to).then(ref => {
              ref.valueChanges().take(1).subscribe((user: User) => {
                if (!user.requestsReceived) {
                  user.requestsReceived = [from];
                } else {
                  if (user.requestsReceived.indexOf(from) == -1) {
                    user.requestsReceived.push(from);
                  }
                }
                ref.update({
                  requestsReceived: user.requestsReceived
                }).then(() => {
                  resolve();
                }).catch(() => {
                  reject();
                });
              });
            }).catch(() => {
              reject();
            });
          }).catch(() => {
            reject();
          });
        });
      }).catch(() => {
        reject();
      });
    });
  }

  // Cancele una solicitud de contacto dado el ID de usuario emisor y receptor.
  public cancelRequest(from: string, to: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.get('users/' + from).then(ref => {
        ref.valueChanges().take(1).subscribe((user: User) => {
          if (user.requestsSent) {
            user.requestsSent.splice(user.requestsSent.indexOf(to), 1);
            if (user.requestsSent.length == 0) {
              user.requestsSent = null;
            }
            ref.update({
              requestsSent: user.requestsSent
            }).then(() => {
              this.get('users/' + to).then(ref => {
                ref.valueChanges().take(1).subscribe((user: User) => {
                  if (user.requestsReceived) {
                    user.requestsReceived.splice(user.requestsReceived.indexOf(from), 1);
                    if (user.requestsReceived.length == 0) {
                      user.requestsReceived = null;
                    }
                    ref.update({
                      requestsReceived: user.requestsReceived
                    }).then(() => {
                      resolve();
                    }).catch(() => {
                      reject();
                    });
                  }
                });
              }).catch(() => {
                reject();
              });
            }).catch(() => {
              reject();
            });
          } else {
            reject();
          }
        });
      }).catch(() => {
        reject();
      });
    });
  }

  // Aceptar una solicitud de contacto dado el ID de usuario emisor y receptor.
  public acceptRequest(from: string, to: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.cancelRequest(from, to).then(() => {
        this.get('users/' + from).then(ref => {
          ref.valueChanges().take(1).subscribe((user: User) => {
            if (!user.contacts) {
              user.contacts = [to];
            } else {
              if (user.contacts.indexOf(to) == -1) {
                user.contacts.push(to);
              }
            }
            ref.update({
              contacts: user.contacts
            }).then(() => {
              this.get('users/' + to).then(ref => {
                ref.valueChanges().take(1).subscribe((user: User) => {
                  if (!user.contacts) {
                    user.contacts = [from];
                  } else {
                    if (user.contacts.indexOf(from) == -1) {
                      user.contacts.push(from);
                    }
                  }
                  ref.update({
                    contacts: user.contacts
                  }).then(() => {
                    resolve();
                  }).catch(() => {
                    reject();
                  });
                });
              }).catch(() => {
                reject();
              });
            }).catch(() => {
              reject();
            });
          });
        }).catch(() => {
          reject();
        });
      }).catch(() => {
        reject();
      });
    });
  }
}
