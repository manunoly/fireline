webpackJsonp([15],{

/***/ 397:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IntroPageModule", function() { return IntroPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__intro__ = __webpack_require__(562);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var IntroPageModule = (function () {
    function IntroPageModule() {
    }
    IntroPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__intro__["a" /* IntroPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__intro__["a" /* IntroPage */]),
                __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], IntroPageModule);
    return IntroPageModule;
}());

//# sourceMappingURL=intro.module.js.map

/***/ }),

/***/ 562:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IntroPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_device__ = __webpack_require__(126);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var IntroPage = (function () {
    function IntroPage(navCtrl, navParams, menuCtrl, device, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.device = device;
        this.platform = platform;
    }
    IntroPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Check if device is on iPhoneX and adjust the scss accordingly.
            if (_this.device.model.indexOf('iPhone10') > -1) {
                _this.iPhoneX = true;
            }
            else {
                _this.iPhoneX = false;
            }
            _this.menuCtrl.enable(false);
        }).catch(function () {
            _this.menuCtrl.enable(false);
        });
    };
    IntroPage.prototype.getSlidesPosition = function () {
        return document.getElementById('slides').offsetTop + 'px';
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Slides */])
    ], IntroPage.prototype, "slides", void 0);
    IntroPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-intro',template:/*ion-inline-start:"D:\Fireline\src\pages\main\intro\intro.html"*/'<ion-content class="no-scroll" [ngClass]="{\'iPhoneX\': iPhoneX}">\n  <ion-slides pager="true" id="slides">\n    <ion-slide>\n      <img src="assets/images/image.png"/>\n      <h3><b>{{ \'intro.slide1.title\' | translate }}</b></h3>\n      <p>{{ \'intro.slide1.text\' | translate }}</p>\n    </ion-slide>\n    <ion-slide>\n      <img src="assets/images/image.png"/>\n      <h3><b>{{ \'intro.slide2.title\' | translate }}</b></h3>\n      <p>{{ \'intro.slide2.text\' | translate }}</p>\n    </ion-slide>\n    <ion-slide>\n      <img src="assets/images/image.png"/>\n      <h3><b>{{ \'intro.slide3.title\' | translate }}</b></h3>\n      <p>{{ \'intro.slide3.text\' | translate }}</p>\n    </ion-slide>\n  </ion-slides>\n  <div class="filler-left" *ngIf="!slides.isBeginning()" [style.top]="getSlidesPosition()"></div>\n  <div class="filler-right" *ngIf="!slides.isEnd()" [style.top]="getSlidesPosition()"></div>\n  <div class="buttons" text-center>\n    <button ion-button color="primary" (click)="navCtrl.setRoot(\'LoginPage\')"><b>{{ \'intro.button.login\' | translate }}</b></button>\n    <button ion-button color="dark" (click)="navCtrl.setRoot(\'RegisterPage\')"><b>{{ \'intro.button.register\' | translate }}</b></button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\Fireline\src\pages\main\intro\intro.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_device__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */]])
    ], IntroPage);
    return IntroPage;
}());

//# sourceMappingURL=intro.js.map

/***/ })

});
//# sourceMappingURL=15.js.map