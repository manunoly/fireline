webpackJsonp([20],{

/***/ 203:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 203;

/***/ }),

/***/ 247:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/auth/create-profile/create-profile.module": [
		580,
		12
	],
	"../pages/auth/login/login.module": [
		574,
		19
	],
	"../pages/auth/register/register.module": [
		575,
		18
	],
	"../pages/auth/update-profile/update-profile.module": [
		576,
		11
	],
	"../pages/loader/loader.module": [
		577,
		17
	],
	"../pages/main/add-member/add-member.module": [
		579,
		2
	],
	"../pages/main/chat/chat.module": [
		578,
		1
	],
	"../pages/main/chats/chats.module": [
		581,
		10
	],
	"../pages/main/contacts/contacts.module": [
		582,
		9
	],
	"../pages/main/group-info/group-info.module": [
		588,
		16
	],
	"../pages/main/group-members/group-members.module": [
		583,
		8
	],
	"../pages/main/group/group.module": [
		591,
		0
	],
	"../pages/main/groups/groups.module": [
		584,
		7
	],
	"../pages/main/intro/intro.module": [
		585,
		15
	],
	"../pages/main/new-conversation/new-conversation.module": [
		586,
		6
	],
	"../pages/main/new-group/new-group.module": [
		587,
		5
	],
	"../pages/main/requests/requests.module": [
		592,
		4
	],
	"../pages/main/search-contact/search-contact.module": [
		590,
		3
	],
	"../pages/main/tabs/tabs.module": [
		589,
		14
	],
	"../pages/main/view-profile/view-profile.module": [
		593,
		13
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 247;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 310:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_fcm__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3____ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__environment_environment__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var NotificationProvider = /** @class */ (function () {
    function NotificationProvider(platform, fcm, auth, firestore, toast, translate, http) {
        this.platform = platform;
        this.fcm = fcm;
        this.auth = auth;
        this.firestore = firestore;
        this.toast = toast;
        this.translate = translate;
        this.http = http;
    }
    // Called after user is logged in to set the pushToken on Firestore.
    NotificationProvider.prototype.init = function () {
        var _this = this;
        if (this.platform.is('cordova')) {
            if (this.subscriptions) {
                for (var i = 0; i < this.subscriptions.length; i++) {
                    this.subscriptions[i].unsubscribe();
                }
            }
            else {
                this.subscriptions = [];
            }
            this.fcm.getToken().then(function (token) {
                _this.firestore.setPushToken(_this.auth.getUserData().userId, token);
                var sub = _this.fcm.onTokenRefresh().subscribe(function (token) {
                    _this.firestore.setPushToken(_this.auth.getUserData().userId, token);
                });
                _this.subscriptions.push(sub);
                // Deeplink when push notification is tapped.
                var subscription = _this.fcm.onNotification().subscribe(function (data) {
                    if (data.wasTapped) {
                        // Notification was received when app is minimized and tapped by the user.
                        if (data.partnerId) {
                            // Open the conversation
                            _this.app.getActiveNavs()[0].popToRoot().then(function () {
                                _this.app.getActiveNavs()[0].parent.select(0);
                                _this.app.getRootNavs()[0].push('ChatPage', { userId: data.partnerId });
                            });
                        }
                        if (data.groupId) {
                            // Open the group conversation
                            _this.app.getRootNavs()[0].popToRoot().then(function () {
                                _this.app.getActiveNavs()[0].parent.select(1);
                                _this.app.getRootNavs()[0].push('GroupPage', { groupId: data.groupId });
                            });
                        }
                        if (data.newContact) {
                            // View user contacts
                            _this.app.getRootNavs()[0].popToRoot().then(function () {
                                _this.app.getActiveNavs()[0].parent.select(2);
                            });
                        }
                        if (data.newRequest) {
                            // View pending user requests
                            _this.app.getRootNavs()[0].popToRoot().then(function () {
                                _this.app.getActiveNavs()[0].parent.select(2);
                                _this.app.getRootNavs()[0].push('RequestsPage');
                            });
                        }
                    }
                    else {
                        //Notification was received while the app is opened or in foreground. In case the user needs to be notified.
                    }
                });
                _this.subscriptions.push(subscription);
            }).catch(function (err) {
                console.log('Error Saving Token: ', JSON.stringify(err));
            });
        }
        else {
            console.error('Cordova not found. Please deploy on actual device or simulator.');
        }
    };
    // Called when the user logged out to clear the pushToken on Firestore.
    NotificationProvider.prototype.destroy = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.platform.is('cordova')) {
                _this.firestore.removePushToken(_this.auth.getUserData().userId);
                resolve();
            }
            else {
                reject();
            }
        });
    };
    // Send a push notification given the pushToken, title, data, and the message.
    NotificationProvider.prototype.sendPush = function (pushToken, title, message, data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var postParams = {
                'notification': {
                    'title': title,
                    'body': message,
                    'sound': 'default',
                    'click_action': 'FCM_PLUGIN_ACTIVITY',
                    'icon': 'fcm_push_icon'
                },
                'data': data,
                'to': pushToken,
                'priority': 'high',
                'restricted_package_name': ''
            };
            var headers = new __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["c" /* HttpHeaders */]({
                'Content-Type': 'application/json',
                'Authorization': 'key=' + __WEBPACK_IMPORTED_MODULE_5__environment_environment__["a" /* Environment */].gcmKey
            });
            var options = { headers: headers };
            _this.http.post('https://fcm.googleapis.com/fcm/send', postParams, options).subscribe(function (response) {
                resolve(response);
            }, function (err) {
                reject(err);
            });
        });
    };
    // Set the app to have access to navigation views for Push Notifications Deeplink.
    NotificationProvider.prototype.setApp = function (app) {
        this.app = app;
    };
    NotificationProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_fcm__["a" /* FCM */],
            __WEBPACK_IMPORTED_MODULE_3____["b" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3____["c" /* FirestoreProvider */],
            __WEBPACK_IMPORTED_MODULE_3____["h" /* ToastProvider */],
            __WEBPACK_IMPORTED_MODULE_3____["i" /* TranslateProvider */],
            __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClient */]])
    ], NotificationProvider);
    return NotificationProvider;
}());

//# sourceMappingURL=notification.js.map

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(375);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 375:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export HttpLoaderFactory */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_auth__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_firestore__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_common_http__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ngx_translate_core__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ngx_translate_http_loader__ = __webpack_require__(571);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ionic_image_loader__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__app_component__ = __webpack_require__(573);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_status_bar__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_keyboard__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_facebook__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_google_plus__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_twitter_connect__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_camera__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_file__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_fcm__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_network__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_device__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_photo_viewer__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__environment_environment__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__providers__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__providers_notification_notification__ = __webpack_require__(310);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



























function HttpLoaderFactory(http) {
    return new __WEBPACK_IMPORTED_MODULE_9__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, "./assets/i18n/", ".json");
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* MyApp */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_8__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_8__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: HttpLoaderFactory,
                        deps: [__WEBPACK_IMPORTED_MODULE_7__angular_common_http__["a" /* HttpClient */]]
                    }
                }),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* MyApp */], __WEBPACK_IMPORTED_MODULE_24__environment_environment__["a" /* Environment */].config, {
                    links: [
                        { loadChildren: '../pages/auth/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/auth/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/auth/update-profile/update-profile.module#UpdateProfilePageModule', name: 'UpdateProfilePage', segment: 'update-profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/loader/loader.module#LoaderPageModule', name: 'LoaderPage', segment: 'loader', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/chat/chat.module#ChatPageModule', name: 'ChatPage', segment: 'chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/add-member/add-member.module#AddMemberPageModule', name: 'AddMemberPage', segment: 'add-member', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/auth/create-profile/create-profile.module#CreateProfilePageModule', name: 'CreateProfilePage', segment: 'create-profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/chats/chats.module#ChatsPageModule', name: 'ChatsPage', segment: 'chats', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/contacts/contacts.module#ContactsPageModule', name: 'ContactsPage', segment: 'contacts', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/group-members/group-members.module#GroupMembersPageModule', name: 'GroupMembersPage', segment: 'group-members', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/groups/groups.module#GroupsPageModule', name: 'GroupsPage', segment: 'groups', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/intro/intro.module#IntroPageModule', name: 'IntroPage', segment: 'intro', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/new-conversation/new-conversation.module#NewConversationPageModule', name: 'NewConversationPage', segment: 'new-conversation', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/new-group/new-group.module#NewGroupPageModule', name: 'NewGroupPage', segment: 'new-group', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/group-info/group-info.module#GroupInfoPageModule', name: 'GroupInfoPage', segment: 'group-info', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/tabs/tabs.module#TabsPageModule', name: 'TabsPage', segment: 'tabs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/search-contact/search-contact.module#SearchContactPageModule', name: 'SearchContactPage', segment: 'search-contact', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/group/group.module#GroupPageModule', name: 'GroupPage', segment: 'group', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/requests/requests.module#RequestsPageModule', name: 'RequestsPage', segment: 'requests', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/view-profile/view-profile.module#ViewProfilePageModule', name: 'ViewProfilePage', segment: 'view-profile', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_4_angularfire2__["a" /* AngularFireModule */].initializeApp(__WEBPACK_IMPORTED_MODULE_24__environment_environment__["a" /* Environment */].firebase),
                __WEBPACK_IMPORTED_MODULE_5_angularfire2_auth__["b" /* AngularFireAuthModule */],
                __WEBPACK_IMPORTED_MODULE_6_angularfire2_firestore__["b" /* AngularFirestoreModule */].enablePersistence(),
                __WEBPACK_IMPORTED_MODULE_10_ionic_image_loader__["b" /* IonicImageLoader */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* MyApp */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_keyboard__["a" /* Keyboard */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_facebook__["a" /* Facebook */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_google_plus__["a" /* GooglePlus */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_twitter_connect__["a" /* TwitterConnect */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_fcm__["a" /* FCM */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_device__["a" /* Device */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_photo_viewer__["a" /* PhotoViewer */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_25__providers__["b" /* AuthProvider */],
                __WEBPACK_IMPORTED_MODULE_25__providers__["a" /* AlertProvider */],
                __WEBPACK_IMPORTED_MODULE_25__providers__["d" /* LoadingProvider */],
                __WEBPACK_IMPORTED_MODULE_25__providers__["h" /* ToastProvider */],
                __WEBPACK_IMPORTED_MODULE_25__providers__["i" /* TranslateProvider */],
                __WEBPACK_IMPORTED_MODULE_25__providers__["c" /* FirestoreProvider */],
                __WEBPACK_IMPORTED_MODULE_25__providers__["g" /* StorageProvider */],
                __WEBPACK_IMPORTED_MODULE_25__providers__["e" /* NetworkProvider */],
                __WEBPACK_IMPORTED_MODULE_26__providers_notification_notification__["a" /* NotificationProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 408:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TranslateProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TranslateProvider = /** @class */ (function () {
    function TranslateProvider() {
    }
    // Set the translations of the app.
    TranslateProvider.prototype.setTranslations = function (translations) {
        this.translations = translations;
    };
    TranslateProvider.prototype.getTranslations = function () {
        return this.translations;
    };
    // Get the translated string given the key based on the i18n .json file.
    TranslateProvider.prototype.get = function (key) {
        return this.translations[key];
    };
    TranslateProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], TranslateProvider);
    return TranslateProvider;
}());

//# sourceMappingURL=translate.js.map

/***/ }),

/***/ 409:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environment_environment__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoadingProvider = /** @class */ (function () {
    function LoadingProvider(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
    }
    // Show the loading indicator.
    LoadingProvider.prototype.show = function () {
        if (!this.loading) {
            var options = __WEBPACK_IMPORTED_MODULE_2__environment_environment__["a" /* Environment */].loading;
            this.loading = this.loadingCtrl.create(options);
            this.loading.present();
        }
    };
    // Hide the loading indicator.
    LoadingProvider.prototype.hide = function () {
        if (this.loading) {
            this.loading.dismiss();
            this.loading = null;
        }
    };
    LoadingProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */]])
    ], LoadingProvider);
    return LoadingProvider;
}());

//# sourceMappingURL=loading.js.map

/***/ }),

/***/ 410:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToastProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environment_environment__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ToastProvider = /** @class */ (function () {
    function ToastProvider(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    // Show a toast message given the message.
    ToastProvider.prototype.show = function (message) {
        var _this = this;
        if (!this.toast) {
            var options = __WEBPACK_IMPORTED_MODULE_2__environment_environment__["a" /* Environment */].toast;
            this.toast = this.toastCtrl.create(options);
            this.toast.setMessage(message);
            this.toast.setDuration(__WEBPACK_IMPORTED_MODULE_2__environment_environment__["a" /* Environment */].toastDuration);
            this.toast.present();
            this.toast.onDidDismiss(function () {
                _this.toast = null;
            });
        }
    };
    // Hide the shown toast message.
    ToastProvider.prototype.hide = function () {
        if (this.toast)
            this.toast.dismiss();
    };
    ToastProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ToastController */]])
    ], ToastProvider);
    return ToastProvider;
}());

//# sourceMappingURL=toast.js.map

/***/ }),

/***/ 411:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AlertProvider = /** @class */ (function () {
    function AlertProvider(alertCtrl) {
        this.alertCtrl = alertCtrl;
    }
    // Show a pop up alert.
    AlertProvider.prototype.showAlert = function (title, subTitle, button) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.alert = _this.alertCtrl.create({
                title: title,
                subTitle: subTitle,
                buttons: [{
                        text: button,
                        role: 'cancel',
                        handler: function () {
                            resolve();
                        }
                    }]
            });
            _this.alert.present();
        });
    };
    // Show a confirmation pop up, returns a boolean if the user has confirmed or canceled.
    AlertProvider.prototype.showConfirm = function (title, subTitle, cancelButton, okButton) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.alert = _this.alertCtrl.create({
                title: title,
                subTitle: subTitle,
                buttons: [
                    {
                        text: cancelButton,
                        role: 'cancel',
                        handler: function () {
                            resolve(false);
                        },
                    },
                    {
                        text: okButton,
                        handler: function () {
                            resolve(true);
                        },
                    }
                ]
            });
            _this.alert.present();
        });
    };
    AlertProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], AlertProvider);
    return AlertProvider;
}());

//# sourceMappingURL=alert.js.map

/***/ }),

/***/ 412:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirestoreProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_firestore__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_take__ = __webpack_require__(488);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_take___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_take__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FirestoreProvider = /** @class */ (function () {
    function FirestoreProvider(afs) {
        this.afs = afs;
    }
    // Obtiene un objeto de Firestore por su ruta. Por ejemplo: firerestore.get ('users /' + userId) para obtener un objeto de usuario.
    FirestoreProvider.prototype.get = function (path) {
        var _this = this;
        return new Promise(function (resolve) {
            resolve(_this.afs.doc(path));
        });
    };
    // Verifica si el objeto existe en Firestore. Devuelve una promesa booleana con verdadero / falso.
    FirestoreProvider.prototype.exists = function (path) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.afs.doc(path).valueChanges().take(1).subscribe(function (res) {
                if (res) {
                    resolve(true);
                }
                else {
                    resolve(false);
                }
            });
        });
    };
    // Obtenga todos los usuarios en orden Firestore por sus primeros Nombres.
    FirestoreProvider.prototype.getUsers = function () {
        return this.afs.collection('users', function (ref) { return ref.orderBy('firstName'); });
    };
    // Obtener todas las conversaciones en Firestore.
    FirestoreProvider.prototype.getConversations = function () {
        return this.afs.collection('conversations');
    };
    // Obtenga todas las conversaciones del usuario en Firestore.
    FirestoreProvider.prototype.getUserConversations = function (userId) {
        return this.afs.collection('users/' + userId + '/conversations');
    };
    // Obtener información de conversación de un usuario con su compañero en Firestore.
    FirestoreProvider.prototype.getUserConversation = function (userId, partnerId) {
        return this.afs.doc('users/' + userId + '/conversations/' + partnerId);
    };
    // Obtener todos los grupos en Firestore.
    FirestoreProvider.prototype.getGroups = function () {
        return this.afs.collection('groups');
    };
    // Obtenga todos los grupos de usuarios en Firestore.
    FirestoreProvider.prototype.getUserGroups = function (userId) {
        return this.afs.collection('users/' + userId + '/groups');
    };
    // Obtener información de grupo de un usuario con el groupId en Firestore.
    FirestoreProvider.prototype.getUserGroup = function (userId, groupId) {
        return this.afs.doc('users/' + userId + '/groups/' + groupId);
    };
    // Obtener userData de un usuario dado el nombre de usuario. Devuelve la promesa userData.
    FirestoreProvider.prototype.getUserByUsername = function (username) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.afs.collection('users', function (ref) { return ref.where('username', '==', username); }).valueChanges().take(1).subscribe(function (res) {
                if (res.length > 0) {
                    resolve(res[0]);
                }
                else {
                    resolve();
                }
            });
        });
    };
    // Obtiene userData de un usuario dado el pushToken. Devuelve la promesa userData.
    FirestoreProvider.prototype.getUserByPushToken = function (token) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.afs.collection('users', function (ref) { return ref.where('pushToken', '==', token); }).valueChanges().take(1).subscribe(function (res) {
                if (res.length > 0) {
                    resolve(res[0]);
                }
                else {
                    resolve();
                }
            });
        });
    };
    // Establece el pushToken del usuario dado el userId.
    FirestoreProvider.prototype.setPushToken = function (userId, token) {
        this.get('users/' + userId).then(function (ref) {
            ref.update({
                pushToken: token
            });
        }).catch(function () { });
    };
    // Eliminar el pushToken del usuario dado el userId.
    FirestoreProvider.prototype.removePushToken = function (userId) {
        this.get('users/' + userId).then(function (ref) {
            ref.update({
                pushToken: ''
            });
        }).catch(function () { });
    };
    // Enviar una solicitud de contacto dado el ID de usuario emisor y receptor.
    FirestoreProvider.prototype.sendRequest = function (from, to) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.get('users/' + from).then(function (ref) {
                ref.valueChanges().take(1).subscribe(function (user) {
                    if (!user.requestsSent) {
                        user.requestsSent = [to];
                    }
                    else {
                        if (user.requestsSent.indexOf(to) == -1) {
                            user.requestsSent.push(to);
                        }
                    }
                    ref.update({
                        requestsSent: user.requestsSent
                    }).then(function () {
                        _this.get('users/' + to).then(function (ref) {
                            ref.valueChanges().take(1).subscribe(function (user) {
                                if (!user.requestsReceived) {
                                    user.requestsReceived = [from];
                                }
                                else {
                                    if (user.requestsReceived.indexOf(from) == -1) {
                                        user.requestsReceived.push(from);
                                    }
                                }
                                ref.update({
                                    requestsReceived: user.requestsReceived
                                }).then(function () {
                                    resolve();
                                }).catch(function () {
                                    reject();
                                });
                            });
                        }).catch(function () {
                            reject();
                        });
                    }).catch(function () {
                        reject();
                    });
                });
            }).catch(function () {
                reject();
            });
        });
    };
    // Cancele una solicitud de contacto dado el ID de usuario emisor y receptor.
    FirestoreProvider.prototype.cancelRequest = function (from, to) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.get('users/' + from).then(function (ref) {
                ref.valueChanges().take(1).subscribe(function (user) {
                    if (user.requestsSent) {
                        user.requestsSent.splice(user.requestsSent.indexOf(to), 1);
                        if (user.requestsSent.length == 0) {
                            user.requestsSent = null;
                        }
                        ref.update({
                            requestsSent: user.requestsSent
                        }).then(function () {
                            _this.get('users/' + to).then(function (ref) {
                                ref.valueChanges().take(1).subscribe(function (user) {
                                    if (user.requestsReceived) {
                                        user.requestsReceived.splice(user.requestsReceived.indexOf(from), 1);
                                        if (user.requestsReceived.length == 0) {
                                            user.requestsReceived = null;
                                        }
                                        ref.update({
                                            requestsReceived: user.requestsReceived
                                        }).then(function () {
                                            resolve();
                                        }).catch(function () {
                                            reject();
                                        });
                                    }
                                });
                            }).catch(function () {
                                reject();
                            });
                        }).catch(function () {
                            reject();
                        });
                    }
                    else {
                        reject();
                    }
                });
            }).catch(function () {
                reject();
            });
        });
    };
    // Aceptar una solicitud de contacto dado el ID de usuario emisor y receptor.
    FirestoreProvider.prototype.acceptRequest = function (from, to) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.cancelRequest(from, to).then(function () {
                _this.get('users/' + from).then(function (ref) {
                    ref.valueChanges().take(1).subscribe(function (user) {
                        if (!user.contacts) {
                            user.contacts = [to];
                        }
                        else {
                            if (user.contacts.indexOf(to) == -1) {
                                user.contacts.push(to);
                            }
                        }
                        ref.update({
                            contacts: user.contacts
                        }).then(function () {
                            _this.get('users/' + to).then(function (ref) {
                                ref.valueChanges().take(1).subscribe(function (user) {
                                    if (!user.contacts) {
                                        user.contacts = [from];
                                    }
                                    else {
                                        if (user.contacts.indexOf(from) == -1) {
                                            user.contacts.push(from);
                                        }
                                    }
                                    ref.update({
                                        contacts: user.contacts
                                    }).then(function () {
                                        resolve();
                                    }).catch(function () {
                                        reject();
                                    });
                                });
                            }).catch(function () {
                                reject();
                            });
                        }).catch(function () {
                            reject();
                        });
                    });
                }).catch(function () {
                    reject();
                });
            }).catch(function () {
                reject();
            });
        });
    };
    FirestoreProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_firestore__["a" /* AngularFirestore */]])
    ], FirestoreProvider);
    return FirestoreProvider;
}());

//# sourceMappingURL=firestore.js.map

/***/ }),

/***/ 490:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_facebook__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_plus__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_twitter_connect__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_firebase__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__environment_environment__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var AuthProvider = /** @class */ (function () {
    function AuthProvider(afAuth, platform, facebook, googlePlus, twitterConnect, translate, firestore) {
        this.afAuth = afAuth;
        this.platform = platform;
        this.facebook = facebook;
        this.googlePlus = googlePlus;
        this.twitterConnect = twitterConnect;
        this.translate = translate;
        this.firestore = firestore;
    }
    // Get the userData from Firestore of the logged in user on Firebase.
    AuthProvider.prototype.getUserData = function () {
        return this.user;
    };
    // Get the authenticated user on Firebase and update the userData variable.
    AuthProvider.prototype.getUser = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.fbSubscription) {
                _this.fbSubscription.unsubscribe();
            }
            _this.fbSubscription = _this.afAuth.authState.subscribe(function (user) {
                // User is logged in on Firebase.
                if (user) {
                    _this.firestore.get('users/' + user.uid).then(function (ref) {
                        if (_this.fsSubscription) {
                            _this.fsSubscription.unsubscribe();
                        }
                        // Update userData variable from Firestore.
                        _this.fsSubscription = ref.valueChanges().subscribe(function (user) {
                            _this.user = user;
                        });
                    }).catch(function () {
                        reject();
                    });
                }
                resolve(user);
            });
        });
    };
    // Change password of the logged in user on Firebase.
    AuthProvider.prototype.changePassword = function (password) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.auth.currentUser.updatePassword(password).then(function (res) {
                resolve(res);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    // Login to Firebase using email and password combination.
    AuthProvider.prototype.loginWithEmail = function (email, password) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.auth.signInWithEmailAndPassword(email, password).then(function (res) {
                resolve(res);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    // Register an account on Firebase with email and password combination.
    AuthProvider.prototype.registerWithEmail = function (email, password) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.auth.createUserWithEmailAndPassword(email, password).then(function (res) {
                resolve(res);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    // Login on Firebase using Facebook.
    AuthProvider.prototype.loginWithFacebook = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.platform.is('cordova')) {
                _this.facebook.login(['public_profile', 'user_friends', 'email']).then(function (res) {
                    var credential = __WEBPACK_IMPORTED_MODULE_7_firebase___default.a.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
                    _this.afAuth.auth.signInWithCredential(credential).then(function (res) {
                        resolve(res);
                    }).catch(function (err) {
                        reject(_this.translate.get(err.code));
                    });
                }).catch(function (err) {
                    //User cancelled, don't show any error.
                    reject();
                });
            }
            else {
                var error = "Cordova not found. Please deploy on actual device or simulator.";
                reject(error);
            }
        });
    };
    // Login on Firebase using Google.
    AuthProvider.prototype.loginWithGoogle = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.platform.is('cordova')) {
                _this.googlePlus.login({
                    'webClientId': __WEBPACK_IMPORTED_MODULE_8__environment_environment__["a" /* Environment */].googleWebClientId
                }).then(function (res) {
                    var credential = __WEBPACK_IMPORTED_MODULE_7_firebase___default.a.auth.GoogleAuthProvider.credential(res.idToken, res.accessToken);
                    _this.afAuth.auth.signInWithCredential(credential).then(function (res) {
                        resolve(res);
                    }).catch(function (err) {
                        reject(_this.translate.get(err.code));
                    });
                }).catch(function (err) {
                    //User cancelled, don't show any error.
                    reject();
                });
            }
            else {
                var error = "Cordova not found. Please deploy on actual device or simulator.";
                reject(error);
            }
        });
    };
    // Login on Firebase using Twitter.
    AuthProvider.prototype.loginWithTwitter = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.platform.is('cordova')) {
                _this.twitterConnect.login().then(function (res) {
                    var credential = __WEBPACK_IMPORTED_MODULE_7_firebase___default.a.auth.TwitterAuthProvider.credential(res.token, res.secret);
                    _this.afAuth.auth.signInWithCredential(credential).then(function (res) {
                        resolve(res);
                    }).catch(function (err) {
                        reject(_this.translate.get(err.code));
                    });
                }).catch(function (err) {
                    //User cancelled, don't show any error.
                    reject();
                });
            }
            else {
                var error = "Cordova not found. Please deploy on actual device or simulator.";
                reject(error);
            }
        });
    };
    // Log the user out from Firebase.
    AuthProvider.prototype.logout = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.auth.signOut().then(function () {
                _this.facebook.logout();
                _this.googlePlus.logout();
                _this.twitterConnect.logout();
                resolve();
            }).catch(function () {
                reject();
            });
        });
    };
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_facebook__["a" /* Facebook */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_plus__["a" /* GooglePlus */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_twitter_connect__["a" /* TwitterConnect */],
            __WEBPACK_IMPORTED_MODULE_6__providers__["i" /* TranslateProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers__["c" /* FirestoreProvider */]])
    ], AuthProvider);
    return AuthProvider;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__translate_translate__ = __webpack_require__(408);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return __WEBPACK_IMPORTED_MODULE_0__translate_translate__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__loading_loading__ = __webpack_require__(409);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_1__loading_loading__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__toast_toast__ = __webpack_require__(410);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_2__toast_toast__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__alert_alert__ = __webpack_require__(411);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_3__alert_alert__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__firestore_firestore__ = __webpack_require__(412);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_4__firestore_firestore__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__auth_auth__ = __webpack_require__(490);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_5__auth_auth__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__storage_storage__ = __webpack_require__(542);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_6__storage_storage__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__network_network__ = __webpack_require__(543);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_7__network_network__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__notification_notification__ = __webpack_require__(310);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_8__notification_notification__["a"]; });
// Add your providers here for easy indexing.









//# sourceMappingURL=index.js.map

/***/ }),

/***/ 542:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StorageProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_camera__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_file__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3____ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var StorageProvider = /** @class */ (function () {
    function StorageProvider(camera, file, loading, toast, translate) {
        this.camera = camera;
        this.file = file;
        this.loading = loading;
        this.toast = toast;
        this.translate = translate;
        // Set profilePhoto specifications based on CameraOptions.
        // For reference: https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-camera/index.html
        this.profilePhoto = {
            quality: 25,
            targetWidth: 288,
            targetHeight: 288,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: true,
            allowEdit: true
        };
        this.photoMessage = {
            quality: 50,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: false,
            allowEdit: false
        };
    }
    // Convert fileURI to Blob.
    StorageProvider.prototype.uriToBlob = function (fileURI) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.file.resolveLocalFilesystemUrl(fileURI).then(function (fileEntry) {
                fileEntry.getParent(function (directoryEntry) {
                    _this.file.readAsArrayBuffer(directoryEntry.nativeURL, fileEntry.name).then(function (data) {
                        var uint8Array = new Uint8Array(data);
                        var buffer = uint8Array.buffer;
                        var blob = new Blob([buffer]);
                        resolve(blob);
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    // Upload an image file provided the userId, cameraOptions, and sourceType.
    StorageProvider.prototype.upload = function (userId, options, sourceType) {
        var _this = this;
        options.sourceType = sourceType;
        return new Promise(function (resolve, reject) {
            // Get the image file from camera or photo gallery.
            _this.camera.getPicture(options).then(function (fileUri) {
                _this.loading.show();
                var fileName = JSON.stringify(fileUri).substr(JSON.stringify(fileUri).lastIndexOf('/') + 1);
                fileName = fileName.substr(0, fileName.length - 1);
                // Append the date string to the file name to make each upload unique.
                fileName = _this.appendDateString(fileName);
                // Convert URI to Blob.
                console.log("File: " + fileName);
                console.log("FileURI: " + fileUri);
                _this.uriToBlob(fileUri).then(function (blob) {
                    var metadata = {
                        'contentType': blob.type
                    };
                    // Upload blob to Firebase storage.
                    __WEBPACK_IMPORTED_MODULE_4_firebase___default.a.storage().ref().child('images/' + userId + '/' + fileName).put(blob, metadata).then(function (snapshot) {
                        var url = snapshot.metadata.downloadURLs[0];
                        _this.loading.hide();
                        resolve(url);
                    }).catch(function (err) {
                        console.log("ERROR STORAGE: " + JSON.stringify(err));
                        _this.loading.hide();
                        reject();
                        _this.toast.show(_this.translate.get('storage.upload.error'));
                    });
                }).catch(function (err) {
                    console.log("ERROR BLOB: " + err);
                    _this.loading.hide();
                    reject();
                    _this.toast.show(_this.translate.get('storage.upload.error'));
                });
            }).catch(function (err) {
                console.log("ERROR CAMERA: " + JSON.stringify(err));
                reject();
                _this.toast.show(_this.translate.get('storage.upload.error'));
            });
        });
    };
    // Delete the uploaded file by the user, given the userId and URL of the file.
    StorageProvider.prototype.delete = function (userId, url) {
        // Get the fileName from the URL.
        var fileName = url.substring(url.lastIndexOf('%2F') + 3, url.lastIndexOf('?'));
        // Check if file really exists on Firebase storage.
        __WEBPACK_IMPORTED_MODULE_4_firebase___default.a.storage().ref().child('images/' + userId + '/' + fileName).getDownloadURL().then(function (res) {
            // Delete file from storage.
            __WEBPACK_IMPORTED_MODULE_4_firebase___default.a.storage().ref().child('images/' + userId + '/' + fileName).delete();
        }).catch(function (err) { });
    };
    // Append the current date as string to the file name.
    StorageProvider.prototype.appendDateString = function (fileName) {
        var name = fileName.substr(0, fileName.lastIndexOf('.')) + '_' + Date.now();
        var extension = fileName.substr(fileName.lastIndexOf('.'), fileName.length);
        return name + '' + extension;
    };
    StorageProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_3____["d" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_3____["h" /* ToastProvider */],
            __WEBPACK_IMPORTED_MODULE_3____["i" /* TranslateProvider */]])
    ], StorageProvider);
    return StorageProvider;
}());

//# sourceMappingURL=storage.js.map

/***/ }),

/***/ 543:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NetworkProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Subject__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_network__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4____ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NetworkProvider = /** @class */ (function () {
    function NetworkProvider(platform, network, toast, translate) {
        var _this = this;
        this.platform = platform;
        this.network = network;
        this.toast = toast;
        this.translate = translate;
        this.subject = new __WEBPACK_IMPORTED_MODULE_2_rxjs_Subject__["Subject"]();
        // Subscribe to network changes.
        this.platform.ready().then(function () {
            _this.subject = new __WEBPACK_IMPORTED_MODULE_2_rxjs_Subject__["Subject"]();
            var self = _this;
            setTimeout(function () {
                self.onlineSubscription = self.network.onConnect().subscribe(function () {
                    self.connected = true;
                    self.toast.hide();
                    self.subject.next(true);
                });
                self.offlineSubscription = self.network.onDisconnect().subscribe(function () {
                    self.connected = false;
                    self.toast.show(self.translate.get('network.offline'));
                    self.subject.next(false);
                });
            }, 1000);
            if (_this.network.type == 'none') {
                _this.connected = false;
            }
            else {
                _this.connected = true;
            }
        }).catch(function () { });
    }
    // Check if network is online or offline.
    NetworkProvider.prototype.online = function () {
        return this.connected;
    };
    NetworkProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_4____["h" /* ToastProvider */],
            __WEBPACK_IMPORTED_MODULE_4____["i" /* TranslateProvider */]])
    ], NetworkProvider);
    return NetworkProvider;
}());

//# sourceMappingURL=network.js.map

/***/ }),

/***/ 573:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__environment_environment__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ionic_image_loader__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_device__ = __webpack_require__(187);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, storage, auth, translate, alert, firestore, network, notification, translateService, imageLoader, menuCtrl, device) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.storage = storage;
        this.auth = auth;
        this.translate = translate;
        this.alert = alert;
        this.firestore = firestore;
        this.network = network;
        this.notification = notification;
        this.translateService = translateService;
        this.imageLoader = imageLoader;
        this.menuCtrl = menuCtrl;
        this.device = device;
        this.initializeApp();
        this.pages = [
            { title: 'Home', component: 'HomePage' },
            { title: 'Page', component: 'BlankPage' }
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        // Set ImageLoader configurations.
        this.imageLoader.spinnerEnabled = false;
        this.imageLoader.fallbackAsPlaceholder = true;
        this.imageLoader.useImg = true;
        this.imageLoader.setFallbackUrl('assets/images/profile.png');
        this.platform.ready().then(function () {
            // Check if device is on iPhoneX and adjust the scss accordingly.
            if (_this.device.model.indexOf('iPhone10') > -1) {
                _this.iPhoneX = true;
            }
            else {
                _this.iPhoneX = false;
            }
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            // Set language of the app.
            _this.translateService.setDefaultLang(__WEBPACK_IMPORTED_MODULE_6__environment_environment__["a" /* Environment */].language);
            _this.translateService.use(__WEBPACK_IMPORTED_MODULE_6__environment_environment__["a" /* Environment */].language);
            _this.translateService.getTranslation(__WEBPACK_IMPORTED_MODULE_6__environment_environment__["a" /* Environment */].language).subscribe(function (translations) {
                _this.translate.setTranslations(translations);
                // LoaderPage is responsible for loading the relevant pages depending on the state of the user.
                _this.rootPage = 'LoaderPage';
            });
        }).catch(function () {
            // User is deploying the app on Browser.
            _this.translateService.setDefaultLang(__WEBPACK_IMPORTED_MODULE_6__environment_environment__["a" /* Environment */].language);
            _this.translateService.use(__WEBPACK_IMPORTED_MODULE_6__environment_environment__["a" /* Environment */].language);
            _this.translateService.getTranslation(__WEBPACK_IMPORTED_MODULE_6__environment_environment__["a" /* Environment */].language).subscribe(function (translations) {
                _this.translate.setTranslations(translations);
                // LoaderPage is responsible for loading the relevant pages depending on the state of the user.
                _this.rootPage = 'LoaderPage';
            });
        });
    };
    MyApp.prototype.openPage = function (page) {
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.logout = function () {
        var _this = this;
        this.alert.showConfirm(this.translate.get('auth.menu.logout.title'), this.translate.get('auth.menu.logout.text'), this.translate.get('auth.menu.logout.button.cancel'), this.translate.get('auth.menu.logout.button.logout')).then(function (confirm) {
            if (confirm) {
                _this.auth.logout().then(function () {
                    _this.menuCtrl.close();
                    _this.notification.destroy();
                    _this.nav.setRoot('LoginPage');
                }).catch(function () { });
            }
        }).catch(function () { });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"D:\Fireline\src\app\app.html"*/'<!-- <ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title><b>{{ \'auth.menu.title\' | translate }}</b></ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content [ngClass]="{\'iPhoneX\': iPhoneX}">\n    <ion-list>\n      <ion-item menuClose *ngFor="let p of pages" (click)="openPage(p)" no-lines>\n        {{p.title}}\n      </ion-item>\n    </ion-list>\n    <div class="profile" *ngIf="auth.getUserData()">\n      <ion-item no-lines tappable (click)="menuCtrl.close(); nav.push(\'UpdateProfilePage\')">\n        <ion-avatar item-start>\n          <img-loader [src]="auth.getUserData().photo"></img-loader>\n        </ion-avatar>\n        <h2 text-uppercase>{{ auth.getUserData().firstName }} {{ auth.getUserData().lastName }}</h2>\n        <p text-lowercase>{{ auth.getUserData().username }}</p>\n      </ion-item>\n      <button ion-button block color="dark" (click)="logout()" [disabled]="!network.online()"><b>{{ \'auth.button.logout\' | translate }}</b></button>\n    </div>\n  </ion-content>\n</ion-menu> -->\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n'/*ion-inline-end:"D:\Fireline\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["b" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["i" /* TranslateProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["a" /* AlertProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["c" /* FirestoreProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["e" /* NetworkProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["f" /* NotificationProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_7_ionic_image_loader__["a" /* ImageLoaderConfig */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_device__["a" /* Device */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 59:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Environment; });
var Environment;
(function (Environment) {
    // Set your app configurations here.
    // For the list of config options, please refer to https://ionicframework.com/docs/api/config/Config/
    Environment.config = {
        mode: 'ios',
        menuType: 'overlay'
    };
    // Set language to use.
    Environment.language = 'es';
    // Firebase Cloud Messaging Server Key.
    // Get your gcmKey on https://console.firebase.google.com, under Overview -> Project Settings -> Cloud Messaging.
    // This is needed to send push notifications.
    Environment.gcmKey = 'AAAAW63DFKs:APA91bG3cNKXAIXte89kelXUmvt08lkCeUcSxyKNXHf90jNoc4nyCnzr8VMFXtmwzEi4OS4LNFmC5sT6g-VSKDIn8t5FJt3D3pDkkK3Ckc4Uy-Z2I5wwMx1XpM5Pm65UiOjXS_xfyR1d';
    // Set to your Firebase app, you can find your credentials on Firebase app console -> Add Web App.
    Environment.firebase = {
        apiKey: "AIzaSyD21WUv8fGMXo3Spf-Do2ch5KsxR0jJTJ4",
        authDomain: "appbrizzi.firebaseapp.com",
        databaseURL: "https://appbrizzi.firebaseio.com",
        projectId: "appbrizzi",
        storageBucket: "appbrizzi.appspot.com",
        messagingSenderId: "403197807591"
    };
    // You can find your googleWebClientId on your Firebase app console -> Authentication -> Sign-in Method -> Google -> Web client ID
    Environment.googleWebClientId = '393757267115-maknmm3b7q8vajvr1h0t5o5k2lld6i6l.apps.googleusercontent.com';
    // Loading Configuration.
    // Please refer to the official Loading documentation here: https://ionicframework.com/docs/api/components/loading/LoadingController/
    Environment.loading = {
        spinner: 'circles'
    };
    // Toast Configuration.
    // Please refer to the official Toast documentation here: https://ionicframework.com/docs/api/components/toast/ToastController/
    Environment.toast = {
        position: 'bottom' // Position of Toast, top, middle, or bottom.
    };
    Environment.toastDuration = 5000; // Duration (in milliseconds) of how long toast messages should show before they are hidden.
})(Environment || (Environment = {}));
//# sourceMappingURL=environment.js.map

/***/ })

},[361]);
//# sourceMappingURL=main.js.map