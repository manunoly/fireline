webpackJsonp([11],{

/***/ 741:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateProfilePageModule", function() { return UpdateProfilePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__update_profile__ = __webpack_require__(902);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_image_loader__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng_elastic__ = __webpack_require__(897);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng_elastic___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng_elastic__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var UpdateProfilePageModule = (function () {
    function UpdateProfilePageModule() {
    }
    UpdateProfilePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__update_profile__["a" /* UpdateProfilePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__update_profile__["a" /* UpdateProfilePage */]),
                __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["b" /* TranslateModule */].forChild(),
                __WEBPACK_IMPORTED_MODULE_4_ionic_image_loader__["b" /* IonicImageLoader */],
                __WEBPACK_IMPORTED_MODULE_5_ng_elastic__["ElasticModule"]
            ],
        })
    ], UpdateProfilePageModule);
    return UpdateProfilePageModule;
}());

//# sourceMappingURL=update-profile.module.js.map

/***/ }),

/***/ 889:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(1);
var forms_1 = __webpack_require__(28);
__webpack_require__(173);
__webpack_require__(381);
var Observable_1 = __webpack_require__(0);
var ElasticDirective = /** @class */ (function () {
    function ElasticDirective(element, ngZone, model) {
        this.element = element;
        this.ngZone = ngZone;
        this.model = model;
        this.onResize = new core_1.EventEmitter();
    }
    ElasticDirective.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.model) {
            return;
        }
        // Listen for changes to the underlying model
        // to adjust the textarea size.
        this.modelSub = this.model
            .valueChanges
            .debounceTime(100)
            .subscribe(function () { return _this.adjust(); });
    };
    ElasticDirective.prototype.ngOnDestroy = function () {
        if (this.modelSub) {
            this.modelSub.unsubscribe();
        }
    };
    ElasticDirective.prototype.ngAfterViewInit = function () {
        var _this = this;
        if (this.isTextarea(this.element.nativeElement)) {
            this.setupTextarea(this.element.nativeElement);
            return;
        }
        var children = Array.from(this.element.nativeElement.children);
        var textareaEl = children.find(function (el) { return _this.isTextarea(el); });
        if (textareaEl) {
            this.setupTextarea(textareaEl);
            return;
        }
        throw new Error('The `fz-elastic` attribute directive must be used on a `textarea` or an element that contains a `textarea`.');
    };
    ElasticDirective.prototype.onInput = function () {
        // This is run whenever the user changes the input.
        this.adjust();
    };
    ElasticDirective.prototype.isTextarea = function (el) {
        return el.tagName === 'TEXTAREA';
    };
    ElasticDirective.prototype.setupTextarea = function (textareaEl) {
        var _this = this;
        this.textareaEl = textareaEl;
        // Set some necessary styles
        var style = this.textareaEl.style;
        style.overflow = 'hidden';
        style.resize = 'none';
        // Listen for window resize events
        this.ngZone.runOutsideAngular(function () {
            Observable_1.Observable.fromEvent(window, 'resize')
                .debounceTime(100)
                .subscribe(function () { return _this.adjust(); });
        });
        // Ensure we adjust the textarea if
        // content is already present
        this.adjust();
    };
    ElasticDirective.prototype.adjust = function () {
        if (!this.textareaEl) {
            return;
        }
        var previousHeight = parseInt(this.textareaEl.style.height, 10);
        var newHeight = this.textareaEl.scrollHeight;
        this.textareaEl.style.height = 'auto';
        this.textareaEl.style.height = newHeight + "px";
        if (previousHeight !== newHeight) {
            // send resize event
            this.onResize.emit(newHeight);
        }
    };
    ElasticDirective.decorators = [
        { type: core_1.Directive, args: [{
                    selector: '[fz-elastic]'
                },] },
    ];
    /** @nocollapse */
    ElasticDirective.ctorParameters = function () { return [
        { type: core_1.ElementRef, },
        { type: core_1.NgZone, },
        { type: forms_1.NgModel, decorators: [{ type: core_1.Optional },] },
    ]; };
    ElasticDirective.propDecorators = {
        'onResize': [{ type: core_1.Output, args: ['on-resize',] },],
        'onInput': [{ type: core_1.HostListener, args: ['input',] },],
    };
    return ElasticDirective;
}());
exports.ElasticDirective = ElasticDirective;


/***/ }),

/***/ 890:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user__ = __webpack_require__(891);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_0__user__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__message__ = __webpack_require__(892);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_1__message__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__conversation__ = __webpack_require__(893);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_2__conversation__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__group__ = __webpack_require__(894);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_3__group__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_conversation__ = __webpack_require__(895);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_4__user_conversation__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__user_group__ = __webpack_require__(896);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_5__user_group__["a"]; });
// Add your models here for easy indexing.






//# sourceMappingURL=index.js.map

/***/ }),

/***/ 891:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
var User = (function () {
    function User(userId, email, firstName, lastName, photo, username, bio, contacts, //userIds of contacts
        requestsSent, //userIds whom you sent a contact request
        requestsReceived, //userIds who sent you a contact request
        conversations, groups, pushToken, notifications) {
        this.userId = userId;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.photo = photo;
        this.username = username;
        this.bio = bio;
        this.contacts = contacts;
        this.requestsSent = requestsSent;
        this.requestsReceived = requestsReceived;
        this.conversations = conversations;
        this.groups = groups;
        this.pushToken = pushToken;
        this.notifications = notifications;
        this.object = {
            userId: userId,
            email: this.email,
            firstName: this.firstName,
            lastName: this.lastName,
            photo: this.photo,
            username: this.username,
            bio: this.bio,
            requestsSent: this.requestsSent,
            requestsReceived: this.requestsReceived,
            pushToken: this.pushToken,
            notifications: this.notifications
        };
    }
    return User;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 892:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Message; });
var Message = (function () {
    function Message(sender, //userId of sender
        type, //0 = text, 1 = image
        message, date) {
        this.sender = sender;
        this.type = type;
        this.message = message;
        this.date = date;
        this.object = {
            sender: sender,
            type: type,
            message: message,
            date: date
        };
    }
    return Message;
}());

//# sourceMappingURL=message.js.map

/***/ }),

/***/ 893:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Conversation; });
var Conversation = (function () {
    function Conversation(conversationId, messages //based on Message Model
    ) {
        this.conversationId = conversationId;
        this.messages = messages; //based on Message Model
        this.object = {
            conversationId: conversationId,
            messages: messages
        };
    }
    return Conversation;
}());

//# sourceMappingURL=conversation.js.map

/***/ }),

/***/ 894:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Group; });
var Group = (function () {
    function Group(groupId, title, photo, members, //userIds of members of the group
        messages, //based on Message Model
        participants //userIds on users who has sent atleast one message
    ) {
        this.groupId = groupId;
        this.title = title;
        this.photo = photo;
        this.members = members;
        this.messages = messages;
        this.participants = participants; //userIds on users who has sent atleast one message
        this.object = {
            groupId: groupId,
            title: title,
            photo: photo,
            members: members,
            messages: messages,
            participants: participants
        };
    }
    return Group;
}());

//# sourceMappingURL=group.js.map

/***/ }),

/***/ 895:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserConversation; });
var UserConversation = (function () {
    function UserConversation(conversationId, messagesRead) {
        this.conversationId = conversationId;
        this.messagesRead = messagesRead;
        this.object = {
            conversationId: conversationId,
            messagesRead: messagesRead
        };
    }
    return UserConversation;
}());

//# sourceMappingURL=user-conversation.js.map

/***/ }),

/***/ 896:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserGroup; });
var UserGroup = (function () {
    function UserGroup(groupId, messagesRead) {
        this.groupId = groupId;
        this.messagesRead = messagesRead;
        this.object = {
            groupId: groupId,
            messagesRead: messagesRead
        };
    }
    return UserGroup;
}());

//# sourceMappingURL=user-group.js.map

/***/ }),

/***/ 897:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var elastic_module_1 = __webpack_require__(898);
exports.ElasticModule = elastic_module_1.ElasticModule;
var elastic_directive_1 = __webpack_require__(889);
exports.ElasticDirective = elastic_directive_1.ElasticDirective;


/***/ }),

/***/ 898:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(1);
var elastic_directive_1 = __webpack_require__(889);
var ElasticModule = /** @class */ (function () {
    function ElasticModule() {
    }
    ElasticModule.decorators = [
        { type: core_1.NgModule, args: [{
                    declarations: [elastic_directive_1.ElasticDirective],
                    exports: [elastic_directive_1.ElasticDirective]
                },] },
    ];
    /** @nocollapse */
    ElasticModule.ctorParameters = function () { return []; };
    return ElasticModule;
}());
exports.ElasticModule = ElasticModule;


/***/ }),

/***/ 902:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_keyboard__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_device__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__models__ = __webpack_require__(890);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var UpdateProfilePage = (function () {
    function UpdateProfilePage(navCtrl, app, navParams, menuCtrl, alertCtrl, actionSheetCtrl, formBuilder, auth, alert, translate, firestore, loading, toast, network, storage, notification, keyboard, camera, device, platform) {
        this.navCtrl = navCtrl;
        this.app = app;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.alertCtrl = alertCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.formBuilder = formBuilder;
        this.auth = auth;
        this.alert = alert;
        this.translate = translate;
        this.firestore = firestore;
        this.loading = loading;
        this.toast = toast;
        this.network = network;
        this.storage = storage;
        this.notification = notification;
        this.keyboard = keyboard;
        this.camera = camera;
        this.device = device;
        this.platform = platform;
        this.nameValidator = __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required
        ]);
        this.usernameValidator = __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].pattern('^[0-z.]{4,20}$'),
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required
        ]);
        this.emailValidator = __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required,
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].email
        ]);
        this.bioValidator = __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required
        ]);
        this.profileForm = formBuilder.group({
            firstName: ['', this.nameValidator],
            lastName: ['', this.nameValidator],
            username: ['', this.usernameValidator],
            email: ['', this.emailValidator],
            bio: ['', this.bioValidator]
        });
    }
    UpdateProfilePage.prototype.keyDownFunction = function (event) {
        // User pressed return on keypad, proceed with updating profile.
        if (event.keyCode == 13) {
            this.keyboard.close();
            this.updateProfile();
        }
    };
    UpdateProfilePage.prototype.onInput = function (username) {
        var _this = this;
        // Check if the username entered on the form is still available.
        this.uniqueUsername = true;
        if (this.profileForm.controls.username.valid && !this.profileForm.controls.username.hasError('required')) {
            this.firestore.getUserByUsername('@' + username.toLowerCase()).then(function (user) {
                if (user && (_this.userId != user.userId)) {
                    _this.uniqueUsername = false;
                }
            }).catch(function () { });
        }
    };
    UpdateProfilePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Check if device is running on android and adjust the scss accordingly.
            if (_this.device.platform == 'Android') {
                _this.android = true;
            }
            else {
                _this.android = false;
            }
        }).catch(function () { });
        // Set placeholder photo, while the user data is loading.
        this.user = new __WEBPACK_IMPORTED_MODULE_7__models__["d" /* User */]('', '', '', '', 'assets/images/profile.png', '', '', [], [], [], null, null, '', true);
        this.auth.getUser().then(function (user) {
            // Check if user is logged in using email and password and show the change password button.
            _this.userId = user.uid;
            if (user.providerData[0].providerId == 'password') {
                _this.hasPassword = true;
            }
            // Get userData from Firestore and update the form accordingly.
            _this.firestore.get('users/' + _this.userId).then(function (ref) {
                _this.subscription = ref.valueChanges().subscribe(function (user) {
                    _this.user = user;
                    _this.hasPushToken = user.notifications;
                    _this.profileForm.setValue({
                        firstName: user.firstName,
                        lastName: user.lastName,
                        username: user.username.substring(1, user.username.length),
                        email: user.email,
                        bio: user.bio
                    });
                    _this.uniqueUsername = true;
                });
            }).catch(function () { });
        }).catch(function () { });
    };
    UpdateProfilePage.prototype.ionViewWillUnload = function () {
        // Unsubscribe to Subscription.
        if (this.subscription)
            this.subscription.unsubscribe();
        // Delete the photo uploaded from storage to preserve Firebase storage space since it's no longer going to be used.
        if (this.auth.getUserData().photo != this.user.photo)
            this.storage.delete(this.user.userId, this.user.photo);
    };
    UpdateProfilePage.prototype.changePassword = function () {
        var _this = this;
        // Allow user to change their password.
        var alert = this.alertCtrl.create({
            title: this.translate.get('auth.profile.password.title'),
            inputs: [
                {
                    name: 'old',
                    placeholder: this.translate.get('auth.profile.password.old'),
                    type: 'password'
                },
                {
                    name: 'new',
                    placeholder: this.translate.get('auth.profile.password.new'),
                    type: 'password'
                },
                {
                    name: 'verify',
                    placeholder: this.translate.get('auth.profile.password.verify'),
                    type: 'password'
                }
            ],
            buttons: [
                {
                    text: this.translate.get('auth.profile.password.button.cancel'),
                    role: 'cancel',
                    handler: function (data) { }
                },
                {
                    text: this.translate.get('auth.profile.password.button.save'),
                    handler: function (data) {
                        // Check if the user has filled all the fields.
                        if (data.old && data.new && data.verify) {
                            _this.loading.show();
                            _this.auth.getUser().then(function (user) {
                                _this.auth.loginWithEmail(user.email, data.old).then(function (res) {
                                    if (data.new == data.verify) {
                                        _this.auth.changePassword(data.new).then(function (res) {
                                            _this.loading.hide();
                                            _this.toast.show(_this.translate.get('auth.profile.password.update'));
                                        }).catch(function (err) {
                                            _this.loading.hide();
                                            _this.toast.show(_this.translate.get('auth.profile.password.error'));
                                        });
                                    }
                                    else {
                                        _this.loading.hide();
                                        _this.toast.show(_this.translate.get('auth.profile.password.mismatch'));
                                    }
                                }).catch(function (err) {
                                    _this.loading.hide();
                                    _this.toast.show(_this.translate.get('auth.profile.password.invalid'));
                                });
                            }).catch(function () { });
                        }
                        else {
                            return false;
                        }
                    }
                }
            ]
        });
        alert.present();
    };
    UpdateProfilePage.prototype.setPhoto = function () {
        var _this = this;
        // Allow user to upload and set their profile photo using their camera or photo gallery.
        if (this.network.online()) {
            this.actionSheetCtrl.create({
                title: this.translate.get('auth.profile.photo.title'),
                buttons: [
                    {
                        text: this.translate.get('auth.profile.photo.take'),
                        role: 'destructive',
                        handler: function () {
                            _this.storage.upload(_this.userId, _this.storage.profilePhoto, _this.camera.PictureSourceType.CAMERA).then(function (url) {
                                // Delete the previous temporary photo uploaded to preserve Firebase storage space.
                                if (_this.auth.getUserData().photo != _this.user.photo) {
                                    _this.storage.delete(_this.user.userId, _this.user.photo);
                                    _this.user.photo = url;
                                }
                                else {
                                    _this.user.photo = url;
                                }
                            }).catch(function () { });
                        }
                    },
                    {
                        text: this.translate.get('auth.profile.photo.gallery'),
                        handler: function () {
                            _this.storage.upload(_this.userId, _this.storage.profilePhoto, _this.camera.PictureSourceType.PHOTOLIBRARY).then(function (url) {
                                // Delete the previous temporary photo uploaded to preserve Firebase storage space.
                                if (_this.auth.getUserData().photo != _this.user.photo) {
                                    _this.storage.delete(_this.user.userId, _this.user.photo);
                                    _this.user.photo = url;
                                }
                                else {
                                    _this.user.photo = url;
                                }
                            }).catch(function () { });
                        }
                    },
                    {
                        text: this.translate.get('auth.profile.photo.cancel'),
                        role: 'cancel',
                        handler: function () { }
                    }
                ]
            }).present();
        }
    };
    UpdateProfilePage.prototype.updateProfile = function () {
        var _this = this;
        // Check if profileForm is valid and username is unique and proceed with updating the profile.
        if (!this.profileForm.valid || !this.uniqueUsername) {
            this.hasError = true;
        }
        else {
            if (this.uniqueUsername) {
                this.loading.show();
                // Delete previous user photo to preserve Firebase storage space, since it's going to be updated to this.user.photo.
                if (this.auth.getUserData().photo != this.user.photo)
                    this.storage.delete(this.auth.getUserData().userId, this.auth.getUserData().photo);
                // Update userData on Firestore.
                this.firestore.get('users/' + this.userId).then(function (ref) {
                    // Formatting the first and last names to capitalized.
                    var firstName = _this.profileForm.value['firstName'].charAt(0).toUpperCase() + _this.profileForm.value['firstName'].slice(1).toLowerCase();
                    var lastName = _this.profileForm.value['lastName'].charAt(0).toUpperCase() + _this.profileForm.value['lastName'].slice(1).toLowerCase();
                    var pushToken;
                    var user = new __WEBPACK_IMPORTED_MODULE_7__models__["d" /* User */](_this.userId, _this.profileForm.value['email'].toLowerCase(), firstName, lastName, _this.user.photo, '@' + _this.profileForm.value['username'].toLowerCase(), _this.profileForm.value['bio'], _this.user.contacts, _this.user.requestsSent, _this.user.requestsReceived, _this.user.conversations, _this.user.groups, '', _this.hasPushToken);
                    ref.update({
                        userId: user.userId,
                        email: user.email,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        photo: user.photo,
                        username: user.username,
                        bio: user.bio,
                        notifications: _this.hasPushToken
                    }).then(function () {
                        // Initialize pushToken to receive push notifications if the user enabled them, otherwise clear pushToken.
                        if (_this.hasPushToken) {
                            _this.notification.init();
                        }
                        else {
                            _this.notification.destroy();
                        }
                        _this.loading.hide();
                        _this.toast.show(_this.translate.get('auth.profile.updated'));
                    }).catch(function () { });
                }).catch(function () { });
            }
        }
    };
    UpdateProfilePage.prototype.logout = function () {
        var _this = this;
        this.alert.showConfirm(this.translate.get('auth.menu.logout.title'), this.translate.get('auth.menu.logout.text'), this.translate.get('auth.menu.logout.button.cancel'), this.translate.get('auth.menu.logout.button.logout')).then(function (confirm) {
            if (confirm) {
                _this.auth.logout().then(function () {
                    _this.menuCtrl.close();
                    _this.notification.destroy();
                    _this.app.getRootNavs()[0].setRoot('LoginPage');
                }).catch(function () { });
            }
        }).catch(function () { });
    };
    UpdateProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-update-profile',template:/*ion-inline-start:"D:\Fireline\src\pages\auth\update-profile\update-profile.html"*/'<ion-header>\n  <ion-navbar hideBackButton="true">\n    <ion-title><b>{{ \'auth.nav.title.profile\' | translate }}</b></ion-title>\n    <ion-buttons end>\n      <button ion-button tappable (click)="logout()"><ion-icon name="icon-logout"></ion-icon></button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content [ngClass]="{\'android\': android}">\n  <div class="profile" text-center>\n    <img-loader [src]="user.photo" tappable (click)="setPhoto()" *ngIf="user"></img-loader>\n  </div>\n  <div class="form">\n    <form [formGroup]="profileForm" (keydown)="keyDownFunction($event)">\n      <ion-list>\n        <ion-item no-lines margin-bottom>\n          <ion-input type="text" formControlName="firstName" [placeholder]="translate.get(\'auth.form.firstName\')"></ion-input>\n        </ion-item>\n        <p text-center *ngIf="(profileForm.controls.firstName.hasError(\'required\') || !profileForm.controls.firstName.valid) && hasError">{{ \'auth.form.error.firstName\' | translate }}</p>\n        <ion-item no-lines margin-bottom>\n          <ion-input type="text" formControlName="lastName" [placeholder]="translate.get(\'auth.form.lastName\')"></ion-input>\n        </ion-item>\n        <p text-center *ngIf="(profileForm.controls.lastName.hasError(\'required\') || !profileForm.controls.lastName.valid) && hasError">{{ \'auth.form.error.lastName\' | translate }}</p>\n        <ion-item no-lines margin-bottom>\n          <ion-input type="text" formControlName="username" [placeholder]="translate.get(\'auth.form.username\')" (input)="onInput($event.target.value)"></ion-input>\n        </ion-item>\n        <p text-center *ngIf="(profileForm.controls.username.hasError(\'required\') || !profileForm.controls.username.valid) && hasError">{{ \'auth.form.error.username\' | translate }}</p>\n        <p text-center *ngIf="!uniqueUsername && profileForm.controls.username.valid && hasError">{{ \'auth.form.error.exists\' | translate }}</p>\n        <ion-item no-lines margin-bottom>\n          <ion-input type="text" formControlName="email" [placeholder]="translate.get(\'auth.form.email\')"></ion-input>\n        </ion-item>\n        <p text-center *ngIf="(profileForm.controls.email.hasError(\'required\') || !profileForm.controls.email.valid) && hasError">{{ \'auth.form.error.email\' | translate }}</p>\n        <ion-item no-lines margin-bottom class="textarea">\n          <ion-textarea type="text" formControlName="bio" [placeholder]="translate.get(\'auth.form.bio\')" fz-elastic rows="3"></ion-textarea>\n        </ion-item>\n        <p text-center *ngIf="profileForm.controls.bio.hasError(\'required\') && hasError">{{ \'auth.form.error.bio\' | translate }}</p>\n        <div text-center *ngIf="hasPassword">\n          <button ion-button color="dark" (click)="changePassword()" [disabled]="!network.online()"><b>{{ \'auth.button.change\' | translate }}</b></button>\n        </div>\n      </ion-list>\n    </form>\n  </div>\n  <div class="toggle">\n    <ion-list>\n      <ion-list-header>\n        <b>{{ \'auth.profile.notifications\' | translate }}</b>\n      </ion-list-header>\n      <ion-item no-lines>\n        <ion-label>{{ \'auth.profile.push\' | translate }}</ion-label>\n        <ion-toggle [(ngModel)]="hasPushToken"></ion-toggle>\n      </ion-item>\n      <div text-center>\n        <button ion-button color="primary" (click)="updateProfile()" [disabled]="!network.online()"><b>{{ \'auth.button.save\' | translate }}</b></button>\n        <button ion-button color="dark" (click)="logout()" [disabled]="!network.online()"><b>{{ \'auth.button.logout\' | translate }}</b></button>\n      </div>\n    </ion-list>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\Fireline\src\pages\auth\update-profile\update-profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers__["b" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["a" /* AlertProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["i" /* TranslateProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["c" /* FirestoreProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["d" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["h" /* ToastProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["e" /* NetworkProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["g" /* StorageProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["f" /* NotificationProvider */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_keyboard__["a" /* Keyboard */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_device__["a" /* Device */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */]])
    ], UpdateProfilePage);
    return UpdateProfilePage;
}());

//# sourceMappingURL=update-profile.js.map

/***/ })

});
//# sourceMappingURL=11.js.map