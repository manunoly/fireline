webpackJsonp([17],{

/***/ 742:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderPageModule", function() { return LoaderPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__loader__ = __webpack_require__(903);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoaderPageModule = (function () {
    function LoaderPageModule() {
    }
    LoaderPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__loader__["a" /* LoaderPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__loader__["a" /* LoaderPage */]),
            ],
        })
    ], LoaderPageModule);
    return LoaderPageModule;
}());

//# sourceMappingURL=loader.module.js.map

/***/ }),

/***/ 903:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoaderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_splash_screen__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoaderPage = (function () {
    function LoaderPage(navCtrl, navParams, splashScreen, storage, auth, firestore, zone) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.splashScreen = splashScreen;
        this.storage = storage;
        this.auth = auth;
        this.firestore = firestore;
        this.zone = zone;
    }
    LoaderPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        // Show the splashScreen while the page to show to the user is still loading.
        this.splashScreen.show();
        this.storage.get('introShown').then(function (introShown) {
            // Check if user is loading the app for the very first time and show the IntroPage.
            if (introShown) {
                // Check if user is authenticated on Firebase or not.
                _this.auth.getUser().then(function (user) {
                    if (!user) {
                        // User is not authenticated, proceed to LoginPage.
                        _this.navCtrl.setRoot('LoginPage');
                        _this.splashScreen.hide();
                    }
                    else {
                        // Check if userData is already created on Firestore.
                        _this.firestore.exists('users/' + user.uid).then(function (exists) {
                            // No data yet, proceed to CreateProfilePage.
                            if (!exists) {
                                _this.navCtrl.setRoot('CreateProfilePage');
                                _this.splashScreen.hide();
                            }
                            else {
                                // Data exists, proceed to TabsPage.
                                _this.zone.run(function () {
                                    _this.navCtrl.setRoot('TabsPage');
                                });
                                _this.splashScreen.hide();
                            }
                        }).catch(function () { });
                    }
                }).catch(function () { });
            }
            else {
                // User is loading the app for the very first time, show IntroPage.
                _this.navCtrl.setRoot('IntroPage');
                _this.splashScreen.hide();
                _this.storage.set('introShown', true);
            }
        }).catch(function () { });
    };
    LoaderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-loader',template:/*ion-inline-start:"D:\Fireline\src\pages\loader\loader.html"*/'<ion-content></ion-content>\n'/*ion-inline-end:"D:\Fireline\src\pages\loader\loader.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["b" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["c" /* FirestoreProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]])
    ], LoaderPage);
    return LoaderPage;
}());

//# sourceMappingURL=loader.js.map

/***/ })

});
//# sourceMappingURL=17.js.map