webpackJsonp([13],{

/***/ 593:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewProfilePageModule", function() { return ViewProfilePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__view_profile__ = __webpack_require__(758);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_image_loader__ = __webpack_require__(186);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ViewProfilePageModule = /** @class */ (function () {
    function ViewProfilePageModule() {
    }
    ViewProfilePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__view_profile__["a" /* ViewProfilePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__view_profile__["a" /* ViewProfilePage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild(),
                __WEBPACK_IMPORTED_MODULE_4_ionic_image_loader__["b" /* IonicImageLoader */]
            ],
        })
    ], ViewProfilePageModule);
    return ViewProfilePageModule;
}());

//# sourceMappingURL=view-profile.module.js.map

/***/ }),

/***/ 758:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ViewProfilePage = /** @class */ (function () {
    function ViewProfilePage(navCtrl, navParams, viewCtrl, firestore, auth, loading, network, notification, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.firestore = firestore;
        this.auth = auth;
        this.loading = loading;
        this.network = network;
        this.notification = notification;
        this.translate = translate;
    }
    ViewProfilePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.subscriptions = [];
        if (!this.userId) {
            this.userId = this.navParams.get('userId');
        }
        // Subscribe to the logged in user on Firestore and sync.
        this.firestore.get('users/' + this.auth.getUserData().userId).then(function (ref) {
            var subscription = ref.valueChanges().subscribe(function (user) {
                _this.currentUser = user;
            });
            _this.subscriptions.push(subscription);
        }).catch(function () { });
        this.loading.show();
        // Subscribe to the user to view on Firestore and sync.
        this.firestore.get('users/' + this.userId).then(function (ref) {
            var subscription = ref.valueChanges().subscribe(function (user) {
                _this.user = user;
                // Subscribe to the user to view's contacts on Firestore and sync.
                if (_this.user.contacts) {
                    for (var i = 0; i < _this.user.contacts.length; i++) {
                        _this.firestore.get('users/' + _this.user.contacts[i]).then(function (ref) {
                            var subscription = ref.valueChanges().subscribe(function (user) {
                                _this.addOrUpdateContact(user);
                            });
                            _this.subscriptions.push(subscription);
                        });
                    }
                }
                else {
                    _this.contacts = null;
                }
                _this.loading.hide();
            });
            _this.subscriptions.push(subscription);
        }).catch(function () {
            _this.loading.hide();
        });
    };
    ViewProfilePage.prototype.ionViewWillUnload = function () {
        // Clear the subscriptions.
        if (this.subscriptions) {
            for (var i = 0; i < this.subscriptions.length; i++) {
                this.subscriptions[i].unsubscribe();
            }
        }
    };
    ViewProfilePage.prototype.header = function (record, recordIndex, records) {
        if (recordIndex == 0) {
            return record;
        }
        return null;
    };
    // Add or update contact data to sync with Firestore.
    ViewProfilePage.prototype.addOrUpdateContact = function (user) {
        if (this.contacts) {
            var index = -1;
            for (var i = 0; i < this.contacts.length; i++) {
                if (user.userId == this.contacts[i].userId) {
                    index = i;
                }
            }
            if (index > -1) {
                this.contacts[index] = user;
            }
            else {
                this.contacts.push(user);
            }
        }
        else {
            this.contacts = [user];
        }
    };
    // Change the user to view and reload the page.
    ViewProfilePage.prototype.setUser = function (userId) {
        this.userId = userId;
        this.contacts = [];
        this.ionViewDidLoad();
    };
    // Return the status of the request between the logged in user and the user viewed.
    ViewProfilePage.prototype.getRequestStatus = function (user) {
        //0 -> Can be requested | 1 -> Request is pending | 2 -> Sent a contact request | 3 -> Is a contact | -1 -> User is the currentUser
        if (this.currentUser) {
            if (this.currentUser.contacts && this.currentUser.contacts.indexOf(user.userId) > -1) {
                return 3;
            }
            else {
                if (user.requestsReceived && user.requestsReceived.indexOf(this.currentUser.userId) > -1) {
                    return 1;
                }
                else if (user.requestsSent && user.requestsSent.indexOf(this.currentUser.userId) > -1) {
                    return 2;
                }
                else if (user.userId == this.currentUser.userId) {
                    return -1;
                }
                else {
                    return 0;
                }
            }
        }
        return 0;
    };
    // Send a contact request to the user.
    ViewProfilePage.prototype.sendRequest = function (user) {
        var _this = this;
        this.loading.show();
        this.firestore.sendRequest(this.auth.getUserData().userId, user.userId).then(function () {
            // Send a push notification to the user.
            if (user.notifications) {
                _this.notification.sendPush(user.pushToken, _this.auth.getUserData().firstName + ' ' + _this.auth.getUserData().lastName, _this.translate.get('push.contact.sent'), { newRequest: true });
            }
            _this.loading.hide();
        }).catch(function () {
            _this.loading.hide();
        });
    };
    // Cancel a pending contact request to the user.
    ViewProfilePage.prototype.cancelRequest = function (userId) {
        var _this = this;
        this.loading.show();
        this.firestore.cancelRequest(this.auth.getUserData().userId, userId).then(function () {
            _this.loading.hide();
        }).catch(function () {
            _this.loading.hide();
        });
    };
    // Reject a contact request received.
    ViewProfilePage.prototype.rejectRequest = function (userId) {
        var _this = this;
        this.loading.show();
        this.firestore.cancelRequest(userId, this.auth.getUserData().userId).then(function () {
            _this.loading.hide();
        }).catch(function () {
            _this.loading.hide();
        });
    };
    // Accept the contact request received.
    ViewProfilePage.prototype.acceptRequest = function (user) {
        var _this = this;
        this.loading.show();
        this.firestore.acceptRequest(user.userId, this.auth.getUserData().userId).then(function () {
            // Send a push notification to the user.
            if (user.notifications) {
                _this.notification.sendPush(user.pushToken, _this.auth.getUserData().firstName + ' ' + _this.auth.getUserData().lastName, _this.translate.get('push.contact.accepted'), { newContact: true });
            }
            _this.loading.hide();
        }).catch(function () {
            _this.loading.hide();
        });
    };
    // Open a chat with the user being viewed.
    ViewProfilePage.prototype.message = function (userId) {
        this.viewCtrl.dismiss(userId);
    };
    ViewProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-view-profile',template:/*ion-inline-start:"D:\Fireline\src\pages\main\view-profile\view-profile.html"*/'<ion-header>\n  <ion-navbar hideBackButton="true">\n    <ion-title *ngIf="user"><b>{{ user.firstName }} {{ user.lastName }}</b></ion-title>\n    <ion-buttons start>\n      <button ion-button tappable (click)="navCtrl.pop()"><ion-icon name="icon-close"></ion-icon></button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div *ngIf="user">\n    <ion-item class="profile" no-lines>\n      <ion-avatar item-start>\n        <img-loader [src]="user.photo"></img-loader>\n      </ion-avatar>\n      <h2 text-uppercase>{{ user.firstName }} {{ user.lastName }}</h2>\n      <p text-lowercase *ngIf="getRequestStatus(user) == 0 || getRequestStatus(user) == 3 || getRequestStatus(user) == -1">{{ user.username }}</p>\n      <p text-lowercase *ngIf="getRequestStatus(user) == 1">{{ \'contact.request.sent\' | translate }}</p>\n      <p text-lowercase *ngIf="getRequestStatus(user) == 2">{{ \'contact.request.received\' | translate }}</p>\n      <button ion-button item-end (click)="sendRequest(user); $event.stopPropagation();" *ngIf="getRequestStatus(user) == 0" [disabled]="!network.online()"><ion-icon name="icon-add-user"></ion-icon></button>\n      <button ion-button item-end color="dark" (click)="cancelRequest(user.userId); $event.stopPropagation();" *ngIf="getRequestStatus(user) == 1" [disabled]="!network.online()"><ion-icon name="icon-close"></ion-icon></button>\n      <button ion-button item-end color="dark" *ngIf="getRequestStatus(user) == 2" (click)="rejectRequest(user.userId); $event.stopPropagation();" [disabled]="!network.online()"><ion-icon name="icon-close"></ion-icon></button>\n      <button ion-button item-end *ngIf="getRequestStatus(user) == 2" (click)="acceptRequest(user); $event.stopPropagation();" [disabled]="!network.online()"><ion-icon name="icon-check"></ion-icon></button>\n      <button ion-button item-end (click)="message(user.userId); $event.stopPropagation();" *ngIf="getRequestStatus(user) == 3" [disabled]="!network.online()"><ion-icon name="icon-message"></ion-icon></button>\n    </ion-item>\n    <ion-list no-margin>\n      <ion-list-header>\n        <b>{{ \'profile.about\' | translate }}</b>\n      </ion-list-header>\n      <p class="bio">{{ user.bio }}</p>\n    </ion-list>\n    <ion-list [virtualScroll]="contacts" [headerFn]="header" approxItemHeight="60px" style="margin-top: 0.5vh">\n      <ion-item-divider *virtualHeader="let header"><b>{{ \'profile.contacts\' | translate }}</b></ion-item-divider>\n      <ion-item no-lines *virtualItem="let user" tappable (click)="setUser(user.userId)">\n        <ion-avatar item-start>\n          <img-loader [src]="user?.photo"></img-loader>\n        </ion-avatar>\n        <h2 text-uppercase>{{ user?.firstName }} {{ user?.lastName }}</h2>\n        <p text-lowercase>{{ user?.username }}</p>\n      </ion-item>\n    </ion-list>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\Fireline\src\pages\main\view-profile\view-profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers__["c" /* FirestoreProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers__["b" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers__["d" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers__["e" /* NetworkProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers__["f" /* NotificationProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers__["i" /* TranslateProvider */]])
    ], ViewProfilePage);
    return ViewProfilePage;
}());

//# sourceMappingURL=view-profile.js.map

/***/ })

});
//# sourceMappingURL=13.js.map