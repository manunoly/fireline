webpackJsonp([14],{

/***/ 756:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageModule", function() { return TabsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs__ = __webpack_require__(917);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TabsPageModule = (function () {
    function TabsPageModule() {
    }
    TabsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__tabs__["a" /* TabsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__tabs__["a" /* TabsPage */]),
            ],
        })
    ], TabsPageModule);
    return TabsPageModule;
}());

//# sourceMappingURL=tabs.module.js.map

/***/ }),

/***/ 917:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TabsPage = (function () {
    function TabsPage(navCtrl, navParams, firestore, auth, notification, app) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.firestore = firestore;
        this.auth = auth;
        this.notification = notification;
        this.app = app;
        this.tab1Root = 'ChatsPage';
        this.tab2Root = 'GroupsPage';
        this.tab3Root = 'ContactsPage';
        this.tab4Root = 'UpdateProfilePage';
    }
    TabsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.subscriptions = [];
        this.userConversations = new Map();
        this.userGroups = new Map();
        // Subscribe to current user data on Firestore and sync.
        this.firestore.get('users/' + this.auth.getUserData().userId).then(function (ref) {
            var subscription = ref.valueChanges().subscribe(function (user) {
                _this.user = user;
            });
            _this.subscriptions.push(subscription);
            // Initialize the push notifications (set pushToken) when user logged in.
            ref.valueChanges().take(1).subscribe(function (user) {
                if (user.notifications) {
                    _this.notification.init();
                    _this.notification.setApp(_this.app);
                }
            });
        }).catch(function () { });
        //Subscribe to userConversations on Firestore and sync.
        var conversationSubscription = this.firestore.getUserConversations(this.auth.getUserData().userId).snapshotChanges().subscribe(function (conversations) {
            var _loop_1 = function (i) {
                var partnerId = conversations[i].payload.doc.id;
                var conversation = conversations[i].payload.doc.data();
                var conversationId = conversation.conversationId;
                var subscription = _this.firestore.getUserConversation(_this.auth.getUserData().userId, partnerId).valueChanges().subscribe(function (userConversation) {
                    _this.userConversations.set(conversationId, userConversation);
                });
                _this.subscriptions.push(subscription);
                // Subscribe to conversation.
                _this.firestore.get('conversations/' + conversationId).then(function (ref) {
                    var subscription = ref.valueChanges().subscribe(function (conversation) {
                        _this.addOrUpdateConversation(conversation);
                    });
                    _this.subscriptions.push(subscription);
                });
            };
            for (var i = 0; i < conversations.length; i++) {
                _loop_1(i);
            }
        });
        this.subscriptions.push(conversationSubscription);
        //Subscribe to userGroups on Firestore and sync.
        var groupSubscription = this.firestore.getUserGroups(this.auth.getUserData().userId).snapshotChanges().subscribe(function (groups) {
            var _loop_2 = function (i) {
                var groupId = groups[i].payload.doc.id;
                var subscription = _this.firestore.getUserGroup(_this.auth.getUserData().userId, groupId).valueChanges().subscribe(function (userGroup) {
                    _this.userGroups.set(groupId, userGroup);
                });
                _this.subscriptions.push(subscription);
                // Subscribe to group.
                _this.firestore.get('groups/' + groupId).then(function (ref) {
                    var subscription = ref.valueChanges().subscribe(function (group) {
                        if (group) {
                            // Check if current user is still a member of the group, if not delete the group.
                            if (group.members.indexOf(_this.auth.getUserData().userId) > -1) {
                                _this.addOrUpdateGroup(group);
                            }
                            else {
                                _this.deleteGroupById(groupId);
                                _this.userGroups.delete(groupId);
                            }
                        }
                        else {
                            // Check if group is already deleted from Firestore, if deleted, delete the group.
                            _this.deleteGroupById(groupId);
                            _this.userGroups.delete(groupId);
                        }
                    });
                    _this.subscriptions.push(subscription);
                });
            };
            for (var i = 0; i < groups.length; i++) {
                _loop_2(i);
            }
        });
        this.subscriptions.push(groupSubscription);
    };
    TabsPage.prototype.ionViewWillUnload = function () {
        // Clear subscriptions.
        if (this.subscriptions) {
            for (var i = 0; i < this.subscriptions.length; i++) {
                this.subscriptions[i].unsubscribe();
            }
            this.conversations = null;
            this.groups = null;
        }
    };
    // Add or update the conversation object.
    TabsPage.prototype.addOrUpdateConversation = function (conversation) {
        if (this.conversations) {
            var index = -1;
            for (var i = 0; i < this.conversations.length; i++) {
                if (conversation.conversationId == this.conversations[i].conversationId) {
                    index = i;
                }
            }
            if (index > -1) {
                this.conversations[index] = conversation;
            }
            else {
                this.conversations.push(conversation);
            }
        }
        else {
            this.conversations = [conversation];
        }
    };
    // Add or update the group object.
    TabsPage.prototype.addOrUpdateGroup = function (group) {
        if (this.groups) {
            var index = -1;
            for (var i = 0; i < this.groups.length; i++) {
                if (group.groupId == this.groups[i].groupId) {
                    index = i;
                }
            }
            if (index > -1) {
                this.groups[index] = group;
            }
            else {
                this.groups.push(group);
            }
        }
        else {
            this.groups = [group];
        }
    };
    // Delete the group given the groupId.
    TabsPage.prototype.deleteGroupById = function (groupId) {
        if (this.groups) {
            var index = -1;
            for (var i = 0; i < this.groups.length; i++) {
                if (groupId == this.groups[i].groupId) {
                    index = i;
                }
            }
            if (index > -1) {
                this.groups.splice(index, 1);
            }
        }
    };
    // Get the number of user requests received.
    TabsPage.prototype.getRequestsReceived = function () {
        if (this.user && this.user.requestsReceived) {
            return this.user.requestsReceived.length;
        }
        return null;
    };
    // Get the total number of unread messages (conversations).
    TabsPage.prototype.getUnreadMessages = function () {
        if (this.conversations) {
            var unread = 0;
            for (var i = 0; i < this.conversations.length; i++) {
                if (this.conversations[i].messages) {
                    unread += this.conversations[i].messages.length - this.userConversations.get(this.conversations[i].conversationId).messagesRead;
                }
            }
            if (unread > 0) {
                return unread;
            }
            else {
                return null;
            }
        }
        else {
            return null;
        }
    };
    // Get the total number of unread messages (groups).
    TabsPage.prototype.getUnreadGroupMessages = function () {
        if (this.groups) {
            var unread = 0;
            for (var i = 0; i < this.groups.length; i++) {
                if (this.groups[i].groupId && this.userGroups.get(this.groups[i].groupId)) {
                    var messagesRead = this.userGroups.get(this.groups[i].groupId).messagesRead;
                    unread += this.groups[i].messages.length - messagesRead;
                }
            }
            if (unread > 0) {
                return unread;
            }
            else {
                return null;
            }
        }
        return null;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("tabs"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* Tabs */])
    ], TabsPage.prototype, "tabs", void 0);
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-tabs',template:/*ion-inline-start:"D:\Fireline\src\pages\main\tabs\tabs.html"*/'<ion-tabs #tabs>\n  <ion-tab [root]="tab1Root" tabIcon="icon-chat" tabBadgeStyle="danger" tabBadge="{{ getUnreadMessages() }}"></ion-tab>\n  <ion-tab [root]="tab2Root" tabIcon="icon-group" tabBadgeStyle="danger" tabBadge="{{ getUnreadGroupMessages() }}"></ion-tab>\n  <ion-tab [root]="tab3Root" tabIcon="icon-search" tabBadgeStyle="danger" tabBadge="{{ getRequestsReceived() }}"></ion-tab>\n  <ion-tab [root]="tab4Root" tabIcon="icon-config"></ion-tab> \n</ion-tabs>\n'/*ion-inline-end:"D:\Fireline\src\pages\main\tabs\tabs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers__["c" /* FirestoreProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers__["b" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers__["f" /* NotificationProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ })

});
//# sourceMappingURL=14.js.map