webpackJsonp([19],{

/***/ 739:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login__ = __webpack_require__(900);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var LoginPageModule = (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__login__["a" /* LoginPage */]),
                __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 900:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_keyboard__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, formBuilder, keyboard, auth, loading, toast, translate, network, menuCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.keyboard = keyboard;
        this.auth = auth;
        this.loading = loading;
        this.toast = toast;
        this.translate = translate;
        this.network = network;
        this.menuCtrl = menuCtrl;
        this.emailValidator = __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required,
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].email
        ]);
        this.passwordValidator = __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required,
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].minLength(4)
        ]);
        this.loginForm = formBuilder.group({
            email: ['', this.emailValidator],
            password: ['', this.passwordValidator]
        });
    }
    LoginPage.prototype.keyDownFunction = function (event) {
        // User pressed return on keypad, proceed with logging in.
        if (event.keyCode == 13) {
            this.keyboard.close();
            this.login();
        }
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        this.menuCtrl.enable(false);
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        // Login using Email and Password.
        if (!this.loginForm.valid) {
            this.hasError = true;
        }
        else {
            this.loading.show();
            this.auth.loginWithEmail(this.loginForm.value['email'], this.loginForm.value['password']).then(function (res) {
                _this.loading.hide();
                _this.navCtrl.setRoot('LoaderPage');
            }).catch(function (err) {
                _this.toast.show(_this.translate.get(err.code));
                _this.loading.hide();
            });
        }
    };
    LoginPage.prototype.loginWithFacebook = function () {
        var _this = this;
        // Login using Facebook.
        this.loading.show();
        this.auth.loginWithFacebook().then(function (res) {
            _this.loading.hide();
            _this.navCtrl.setRoot('LoaderPage');
        }).catch(function (err) {
            _this.toast.show(err);
            _this.loading.hide();
        });
    };
    LoginPage.prototype.loginWithGoogle = function () {
        var _this = this;
        // Login using Google.
        this.loading.show();
        this.auth.loginWithGoogle().then(function (res) {
            _this.loading.hide();
            _this.navCtrl.setRoot('LoaderPage');
        }).catch(function (err) {
            _this.toast.show(err);
            _this.loading.hide();
        });
    };
    LoginPage.prototype.loginWithTwitter = function () {
        var _this = this;
        // Login using Twitter.
        this.loading.show();
        this.auth.loginWithTwitter().then(function (res) {
            _this.loading.hide();
            _this.navCtrl.setRoot('LoaderPage');
        }).catch(function (err) {
            _this.toast.show(err);
            _this.loading.hide();
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"D:\Fireline\src\pages\auth\login\login.html"*/'<ion-content class="no-scroll">\n  <div class="logo" text-center>\n    <img src="assets/images/logo.png" tappable (click)="navCtrl.setRoot(\'IntroPage\')"/>\n  </div>\n  <div class="form">\n    <form [formGroup]="loginForm" (keydown)="keyDownFunction($event)">\n      <ion-list>\n        <ion-item no-lines margin-bottom>\n          <ion-input type="email" formControlName="email" [placeholder]="translate.get(\'auth.form.email\')"></ion-input>\n        </ion-item>\n        <p text-center *ngIf="(loginForm.controls.email.hasError(\'required\') || !loginForm.controls.email.valid) && hasError">{{ \'auth.form.error.email\' | translate }}</p>\n        <ion-item no-lines margin-bottom>\n          <ion-input type="password" formControlName="password" [placeholder]="translate.get(\'auth.form.password\')"></ion-input>\n        </ion-item>\n        <p text-center *ngIf="(loginForm.controls.password.hasError(\'required\') || !loginForm.controls.password.valid) && hasError">{{ \'auth.form.error.password\' | translate }}</p>\n        <div text-center>\n          <button ion-button color="primary" (click)="login()" [disabled]="!network.online()"><b>{{ \'auth.button.login\' | translate }}</b></button>\n          <p>{{ \'auth.login.text\' | translate }}</p>\n          <button ion-button color="dark" (click)="loginWithFacebook()" [disabled]="!network.online()"><ion-icon name="icon-facebook"></ion-icon><b>{{ \'auth.button.facebook\' | translate }}</b></button>\n          <!-- <button ion-button color="dark" (click)="loginWithGoogle()" [disabled]="!network.online()"><ion-icon name="icon-google"></ion-icon><b>{{ \'auth.button.google\' | translate }}</b></button> -->\n          <!-- <button ion-button color="dark" (click)="loginWithTwitter()" [disabled]="!network.online()"><ion-icon name="icon-twitter"></ion-icon><b>{{ \'auth.button.twitter\' | translate }}</b></button> -->\n          <p>{{ \'auth.login.question\' | translate }}<span (click)="navCtrl.setRoot(\'RegisterPage\')">{{ \'auth.login.question.button\' | translate }}</span></p>\n        </div>\n      </ion-list>\n    </form>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\Fireline\src\pages\auth\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_keyboard__["a" /* Keyboard */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["b" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["d" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["h" /* ToastProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["i" /* TranslateProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["e" /* NetworkProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

});
//# sourceMappingURL=19.js.map